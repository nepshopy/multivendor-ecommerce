<?php

namespace App\Http\Controllers;

use App\franchise;
use App\User;
use ImageOptimizer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class FranchiseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function apply_for_franchise()
    {
        return view ('franchise.shop');
    }

    public function store_franchise_user(Request $request)
    {
     if(User::where('email', $request->email)->first() != null){
        flash(__('Email already exists!'))->error();
        return back();
    }
    $user = new User;
    $user->name = $request->name;
    $user->email = $request->email;
    $user->phone = $request->phone;
    $user->address = $request->address;
    $user->user_type = "franchise";
    if($request->hasFile('avatar')){
        $user->avatar = $request->avatar->store('uploads');
    }
    $user->password = Hash::make($request->password);
    if($user->save()){
        $franchise = new franchise;
        $franchise->user_id = $user->id;
        $franchise->referal_code = Str::upper(substr($user->name, 0, 4).'-'.Str::random(4));
        $franchise->franchise_state_id = "1";
        if($request->hasFile('citizenship_front')){
            $franchise->citizenship_front = $request->citizenship_front->store('uploads/franchise');
        }
        if($request->hasFile('citizenship_back')){
            $franchise->citizenship_back = $request->citizenship_back->store('uploads/franchise');
        }
        if($franchise->save()){
            flash(__('Thank you for registering to our franchise account, we will contact to you soon ....'))->success();
            return redirect()->back();
        }
    }
    flash(__('Something went wrong'))->error();
    return back();
    }
    public function users()
    {
        return ("hello");
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\franchise  $franchise
     * @return \Illuminate\Http\Response
     */
    public function show(franchise $franchise)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\franchise  $franchise
     * @return \Illuminate\Http\Response
     */
    public function edit(franchise $franchise)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\franchise  $franchise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, franchise $franchise)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\franchise  $franchise
     * @return \Illuminate\Http\Response
     */
    public function destroy(franchise $franchise)
    {
        //
    }
}
