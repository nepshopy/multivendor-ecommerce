<?php

/*
|--------------------------------------------------------------------------
| franchise Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Admin
Route::group(['prefix' =>'admin', 'middleware' => ['auth', 'admin']], function(){
    Route::get('/franchise', 'FranchiseController@index')->name('franchise.index');
    Route::post('/franchise/franchise_option_store', 'FranchiseController@franchise_option_store')->name('franchise.store');

    Route::get('/franchise/configs', 'FranchiseController@configs')->name('franchise.configs');
    Route::post('/franchise/configs/store', 'FranchiseController@config_store')->name('franchise.configs.store');

    Route::get('/franchise/users', 'FranchiseController@users')->name('franchise.users');
    Route::get('/franchise/verification/{id}', 'FranchiseController@show_verification_request')->name('franchise_users.show_verification_request');

    Route::get('/franchise/approve/{id}', 'FranchiseController@approve_user')->name('franchise_user.approve');
	Route::get('/franchise/reject/{id}', 'FranchiseController@reject_user')->name('franchise_user.reject');

    Route::post('/franchise/approved', 'FranchiseController@updateApproved')->name('franchise_user.approved');

    Route::post('/franchise/payment_modal', 'FranchiseController@payment_modal')->name('franchise_user.payment_modal');
    Route::post('/franchise/pay/store', 'FranchiseController@payment_store')->name('franchise_user.payment_store');

    Route::get('/franchise/payments/show/{id}', 'FranchiseController@payment_history')->name('franchise_user.payment_history');
    Route::get('/refferal/users', 'FranchiseController@refferal_users')->name('refferals.users');

});

//FrontEnd
Route::get('/franchise', 'FranchiseController@apply_for_franchise')->name('franchise.apply');
Route::post('/franchise/store', 'FranchiseController@store_franchise_user')->name('franchise.store_franchise_user');
Route::get('/franchise/user', 'FranchiseController@user_index')->name('franchise.user.index');

Route::get('/franchise/payment/settings', 'FranchiseController@payment_settings')->name('franchise.payment_settings');
Route::post('/franchise/payment/settings/store', 'FranchiseController@payment_settings_store')->name('franchise.payment_settings_store');
