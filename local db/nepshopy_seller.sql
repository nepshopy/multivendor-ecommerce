-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2020 at 10:38 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nepshopy_seller`
--

-- --------------------------------------------------------

--
-- Table structure for table `addons`
--

CREATE TABLE `addons` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `unique_identifier` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `version` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `activated` int(1) NOT NULL DEFAULT 1,
  `image` varchar(1000) COLLATE utf32_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `addons`
--

INSERT INTO `addons` (`id`, `name`, `unique_identifier`, `version`, `activated`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Point of Sale', 'pos_system', '1.1', 1, 'pos_banner.jpg', '2020-07-30 04:37:51', '2020-07-30 04:37:51'),
(2, 'refund', 'refund_request', '1.0', 1, 'refund_request.png', '2020-08-04 11:26:06', '2020-08-04 11:26:06');

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `set_default` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `user_id`, `address`, `country`, `city`, `postal_code`, `phone`, `set_default`, `created_at`, `updated_at`) VALUES
(1, 3, 'hlmHS734Sm', 'Afghanistan', 'BcMNW1UEPd', 'KnXwcUbSw5', '8146216865', 0, '2020-07-11 23:19:14', '2020-07-11 23:19:14'),
(2, 8, 'K4aR1zc83d', 'Nepal', 'Gr967GA03D', 'NtM5qOVSGP', '3142309476', 0, '2020-07-12 01:39:12', '2020-07-12 01:39:12'),
(3, 12, 'Imdol, Gwarko, Lalitpur.', 'Nepal', 'Lalitpur', '1231', '9851275055', 0, '2020-07-13 01:44:23', '2020-07-13 01:44:23'),
(4, 22, 'Dharam colony\nPalam Vihar Extension', 'Nepal', 'GURGAON', '122017', '8010198601', 0, '2020-07-16 11:50:27', '2020-07-16 11:50:27'),
(5, 26, 'qwerty', 'Nepal', 'asdf', '123123', '965465132132', 0, '2020-07-17 06:57:54', '2020-07-17 06:57:54'),
(6, 21, 'zbnm,', 'Nepal', 'xsH7MqquAE', 'iNDAB4CiJ2', 'qliBTP0zjg', 0, '2020-07-17 07:47:55', '2020-07-17 07:47:55'),
(7, 27, 'balkot', 'Nepal', 'bhaktapur', '66300', '9843083493', 1, '2020-07-17 09:30:25', '2020-07-17 09:43:18'),
(8, 28, 'Gwarkho', 'Nepal', 'lalitpur', '44600', '9843083493', 0, '2020-07-30 06:12:40', '2020-07-30 06:12:40'),
(9, 33, 'Ratamakai chwok, Gwarkho', 'Nepal', 'lalitpur', '44600', '1234567890', 1, '2020-08-18 10:19:26', '2020-08-18 10:19:53');

-- --------------------------------------------------------

--
-- Table structure for table `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `currency_format` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_plus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `app_settings`
--

INSERT INTO `app_settings` (`id`, `name`, `logo`, `currency_id`, `currency_format`, `facebook`, `twitter`, `instagram`, `youtube`, `google_plus`, `created_at`, `updated_at`) VALUES
(1, 'Active eCommerce', 'uploads/logo/matggar.png', 1, 'symbol', 'https://facebook.com', 'https://twitter.com', 'https://instagram.com', 'https://youtube.com', 'https://google.com', '2019-08-04 16:39:15', '2019-08-04 16:39:18');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `att_property` int(11) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `att_property`, `created_at`, `updated_at`) VALUES
(1, 'Size', 1, '2020-02-24 05:55:07', '2020-02-24 05:55:07'),
(2, 'Fabric', 1, '2020-02-24 05:55:13', '2020-02-24 05:55:13'),
(4, 'Memory', 1, '2020-07-12 18:56:32', '2020-07-12 18:56:32'),
(5, 'K.G', 1, '2020-07-12 19:25:46', '2020-07-12 19:25:46'),
(6, 'Grams', 1, '2020-07-12 19:26:08', '2020-07-12 19:26:08'),
(7, 'Screen Size', 1, '2020-07-12 19:26:42', '2020-07-12 19:26:42'),
(8, 'Litre', 1, '2020-07-12 19:26:58', '2020-07-13 13:16:20'),
(9, 'Storage', 1, '2020-07-30 06:39:01', '2020-07-30 06:39:01'),
(11, 'Weight', 1, '2020-08-01 04:29:22', '2020-08-01 04:29:40'),
(12, 'manual', 0, '2020-08-02 23:33:37', '2020-08-02 23:33:37'),
(13, 'manual options', 0, '2020-08-02 23:33:54', '2020-08-02 23:33:54'),
(14, 'Dress Length', 0, '2020-08-04 14:58:24', '2020-08-04 14:58:24'),
(15, 'Collar Type', 0, '2020-08-04 14:59:01', '2020-08-04 14:59:01'),
(16, 'Model', 0, '2020-08-04 14:59:22', '2020-08-04 14:59:22'),
(17, 'Pattern', 0, '2020-08-04 14:59:37', '2020-08-04 14:59:37'),
(18, 'Dress Shape', 0, '2020-08-04 14:59:50', '2020-08-04 14:59:50'),
(19, 'Blouse Sleeve Style', 0, '2020-08-04 15:00:07', '2020-08-04 15:00:07'),
(20, 'Material Family', 0, '2020-08-04 15:00:34', '2020-08-04 15:00:34'),
(21, 'Area Of Use', 0, '2020-08-04 15:00:51', '2020-08-04 15:00:51'),
(22, 'Designer', 0, '2020-08-04 15:01:09', '2020-08-04 15:01:09'),
(23, 'Color Name Brand', 0, '2020-08-04 15:01:24', '2020-08-04 15:01:24'),
(24, 'Main Material', 0, '2020-08-04 15:01:36', '2020-08-04 15:01:36'),
(25, 'Appareltype', 0, '2020-08-04 15:01:49', '2020-08-04 15:01:49'),
(26, 'Campaign', 0, '2020-08-04 15:02:02', '2020-08-04 15:02:02'),
(27, 'Season', 0, '2020-08-04 15:02:15', '2020-08-04 15:02:15'),
(28, 'Collection', 0, '2020-08-04 15:02:26', '2020-08-04 15:02:26'),
(29, 'Listed Year Season', 0, '2020-08-04 15:02:40', '2020-08-04 15:02:40'),
(30, 'Clothing Style', 0, '2020-08-04 15:02:52', '2020-08-04 15:02:52'),
(31, 'Women\'s Trend', 0, '2020-08-04 15:03:06', '2020-08-04 15:03:06'),
(32, 'Sleeves', 0, '2020-08-04 15:03:18', '2020-08-04 15:03:18'),
(33, 'Clothing Material', 0, '2020-08-04 15:03:29', '2020-08-04 15:03:29'),
(34, 'Age Range', 0, '2020-08-04 15:04:16', '2020-08-04 15:04:16'),
(35, 'Bracelet Size', 0, '2020-08-04 15:04:31', '2020-08-04 15:04:31'),
(36, 'Complementary Products', 0, '2020-08-04 15:04:51', '2020-08-04 15:04:51'),
(37, 'Color Code Brand', 0, '2020-08-04 15:05:08', '2020-08-04 15:05:08'),
(38, 'Fashion Features', 0, '2020-08-04 15:05:21', '2020-08-04 15:05:21'),
(39, 'Chain Size', 0, '2020-08-04 15:05:35', '2020-08-04 15:05:35'),
(40, 'Features', 0, '2020-08-04 15:05:54', '2020-08-04 15:05:54');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT 1,
  `published` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `photo`, `url`, `position`, `published`, `created_at`, `updated_at`) VALUES
(4, 'uploads/banners/banner.jpg', '#', 1, 1, '2019-03-12 05:58:23', '2019-06-11 04:56:50'),
(5, 'uploads/banners/banner.jpg', '#', 1, 1, '2019-03-12 05:58:41', '2019-03-12 05:58:57'),
(6, 'uploads/banners/banner.jpg', '#', 2, 1, '2019-03-12 05:58:52', '2019-03-12 05:58:57'),
(7, 'uploads/banners/banner.jpg', '#', 2, 1, '2019-05-26 05:16:38', '2019-05-26 05:17:34'),
(8, 'uploads/banners/banner.jpg', '#', 2, 1, '2019-06-11 05:00:06', '2019-06-11 05:00:27'),
(9, 'uploads/banners/banner.jpg', '#', 1, 1, '2019-06-11 05:00:15', '2019-06-11 05:00:29'),
(10, 'uploads/banners/banner.jpg', '#', 1, 0, '2019-06-11 05:00:24', '2019-06-11 05:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top` int(1) NOT NULL DEFAULT 0,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `logo`, `top`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 'Acer', 'uploads/brands/M9mBFVw1EHA2hGJf4SHQD7dVReFyWbnvJ4QQQgiz.jpeg', 1, 'acer', 'acer', 'all Acer items', '2019-03-12 06:05:56', '2020-08-12 19:37:34'),
(2, 'Hp', 'uploads/brands/5EvztnjmWOsIHjpl5Ye8MdKqWBI3pnxfqsO4qLqa.jpeg', 1, 'hp', 'hp', 'all Hp items', '2019-03-12 06:06:13', '2020-08-12 19:37:19'),
(5, 'Dell', 'uploads/brands/bAkLcBzwEkEDHhV08oYn8Z6OcDZn80sy5JOypOvx.jpeg', 1, 'dell', 'dell', 'all dell items', '2020-07-12 15:53:29', '2020-08-12 19:37:06'),
(6, 'Lg', 'uploads/brands/gadk3FVuPt3ZVCF8FfRuoaZFWaPPxuzYa05FH437.jpeg', 0, 'lg', 'lg', 'all Lg brand', '2020-07-12 15:53:44', '2020-08-12 19:36:50'),
(7, 'Apple', 'uploads/brands/KXo7wFZ1HBYy4bKHbFRl4ZvE2krMprQUHFekZvz1.jpeg', 1, 'apple', 'apple', 'all apple brands', '2020-07-12 15:54:03', '2020-08-12 19:36:25'),
(8, 'Samsung', 'uploads/brands/zbLOKtxeKRavpxEXp8rdfHBX6hPbaBvrbhthsK1k.jpeg', 1, 'samsung', 'samsung', 'all samsung brands', '2020-07-12 15:54:48', '2020-08-12 19:36:06'),
(9, 'sony', 'uploads/brands/4ksvz7ZxIZHKvx1nhzkKYPo3lvdsJRJ9QXPYuqH3.jpeg', 1, 'sony', 'sonyinc', 'sony', '2020-07-12 15:54:58', '2020-08-12 19:35:54'),
(10, 'Asus', 'uploads/brands/55WfAtf8xHjPHGiYVGMFuyfKvE0TkG6nb0SBsjfr.jpeg', 0, 'Asus-BEKOG', 'Asus', 'all Asus item', '2020-08-12 20:13:54', '2020-08-12 20:13:54'),
(11, 'blackberry', 'uploads/brands/GxpN2yX7tJsfDnEFg3MSzWDJ8t20WFyKRWSK59en.jpeg', 0, 'blackberry-LQhvO', 'Blackberry', 'all Blackberry item', '2020-08-12 20:15:23', '2020-08-12 20:15:23'),
(13, 'Bosch', 'uploads/brands/B28aJ49QQFUQFBgLbxijlJX5SQYrtao5BlzGLX2x.jpeg', 0, 'Bosch-gjGHg', 'Bosch', 'all Bosch item', '2020-08-12 20:16:18', '2020-08-12 20:16:18'),
(14, 'Honor', 'uploads/brands/ec4lfS3sXsCgloQQ4D2mm8ZqdKvKlel3NeJK4dT9.jpeg', 0, 'Honor-DAgd8', 'Honor', 'all Honor item', '2020-08-12 20:17:01', '2020-08-12 20:17:01'),
(15, 'Huawei', 'uploads/brands/faQZH1XAKJZjVexIrdfGTY9pVb1lBRwWH42qZcDm.jpeg', 0, 'Huawei-ug6za', 'Huawei', 'all Huawei item', '2020-08-12 20:18:49', '2020-08-12 20:18:49'),
(16, 'Lenovo', 'uploads/brands/uKyJkSZ24HprPYrqUICV3jD9C1z3UyI4kzHgCbna.jpeg', 0, 'Lenovo-VxMMZ', 'Lenovo', 'all Lenovo item', '2020-08-12 20:19:51', '2020-08-12 20:19:51'),
(17, 'Micromax', 'uploads/brands/DFWfRN5zeIUswQo56sXtCFbUVDGTSXTicd9UGAi3.jpeg', 0, 'Micromax-ehqyp', 'Micromax', 'All Micromax item', '2020-08-12 20:20:22', '2020-08-12 20:20:22'),
(18, 'Microsoft', 'uploads/brands/zmcWLhoAaHbSREPXWmUGey3wNe8sIMvALYaW3To8.jpeg', 0, 'Microsoft-1dALm', 'Microsoft', 'all Microsoft items', '2020-08-12 20:21:10', '2020-08-12 20:21:10'),
(19, 'Motorola', 'uploads/brands/qE8v4xbiqQX2XICdo0IyE5ZHT2OcObmpwk1atDsF.jpeg', 0, 'Motorola-6XHEA', 'Motorola', 'all Motorola items', '2020-08-12 20:21:42', '2020-08-12 20:21:42'),
(20, 'Nokia', 'uploads/brands/vlsDMrjhN70CjdCh0qnN4LudSiJAj2pdv1uBNrn8.jpeg', 0, 'Nokia-z8Eg8', 'Nokia', 'all Nokia items', '2020-08-12 20:22:20', '2020-08-12 20:22:20'),
(21, 'Oneplus', 'uploads/brands/eXphF9ZL5v2rm2x7ptt0Bui0XvJBe8Qhs2nAx7iQ.jpeg', 0, 'Oneplus-qyjsM', 'Oneplus', 'all Oneplus item', '2020-08-12 20:22:57', '2020-08-12 20:22:57'),
(22, 'Panasonic', 'uploads/brands/lBbgXrrW6S3Bm1Tv6UpNMGnGnpxQtiTZeZaXE5pd.jpeg', 0, 'Panasonic-9Yyah', 'Panasonic', 'all Panasonic item', '2020-08-12 20:23:40', '2020-08-12 20:23:40'),
(24, 'Xiaomi', 'uploads/brands/bLZzXT668IPNsjlgER32awHUVrpnuTEmCbTvACVe.jpeg', 0, 'Xiaomi-eSp0L', 'Xiaomi', 'all Xiaomi items', '2020-08-12 20:24:38', '2020-08-12 20:24:38'),
(25, 'Vivo', 'uploads/brands/6XuD45PAjC3MEXzjs4vibyiaz6xEbrtOPdfAiMpj.jpeg', 0, 'Vivo-0rPEA', 'VIvo', 'all Vivo item', '2020-08-12 20:25:58', '2020-08-12 20:25:58'),
(26, 'Vodafone', 'uploads/brands/imZV7mH8C29xnlBrM9IIXJ6dzMSOlDqUByZjFOCz.jpeg', 0, 'Vodafone-QEinn', 'Vodafone', 'all Vodafone item', '2020-08-12 20:26:25', '2020-08-12 20:26:25'),
(27, 'Beats', 'uploads/brands/zs2U0KFZnEP4giY6Fa1CLWyK08m4lXGcA0FoZBPS.jpeg', 0, 'Beats-hXNeM', 'Beats', 'all Beats item', '2020-08-12 20:50:37', '2020-08-12 20:50:37'),
(29, 'Canon', 'uploads/brands/3GXX8ajscFXmiNdcVvw6qPpLk2DUxkl6spkryO1v.jpeg', 0, 'Canon-qJPBT', 'Canon', 'all Canon item', '2020-08-12 20:51:02', '2020-08-12 20:51:02'),
(30, 'GoPro', 'uploads/brands/2MIuqTDEmwXmgoBVhT96Tihoq47w35eYbkFaMkzn.jpeg', 0, 'GoPro-HxylM', 'GoPro', 'all GoPro item', '2020-08-12 20:51:48', '2020-08-12 20:51:48'),
(31, 'Nikon', 'uploads/brands/rPx1YeLmazmEFSDutrHhqWLlWOxlmocpbD2NpItP.jpeg', 0, 'Nikon-dMmeG', 'Nikon', 'all Nikon item', '2020-08-12 20:52:50', '2020-08-12 20:52:50'),
(32, 'SanDisk', 'uploads/brands/PXqyE0J9NwL6DexsijqaSZk0LWsQaFRWG8bUGZp3.jpeg', 0, 'SanDisk-Xklmz', 'SanDisk', 'all SanDisk item', '2020-08-12 20:53:36', '2020-08-12 20:53:36'),
(33, 'Rebok', 'uploads/brands/kgRvozIGcZX3HIGjWkoco23uXndfklFWrMjuCfjt.jpeg', 0, 'Rebok-cY1dv', 'Rebok', 'all Rebok item', '2020-08-12 21:05:55', '2020-08-12 21:05:55'),
(34, 'D & G', 'uploads/brands/kGQGO8zVHERwzFFS8nzMyMancAenUIMBqXtkwzLw.jpeg', 0, 'D--G-hWRcO', 'D & G', 'D & G items', '2020-08-12 21:30:39', '2020-08-12 21:30:39'),
(35, 'vans', 'uploads/brands/rU01B0CgfOU9FUE3BWjsiUP7h3AF00SSOXhm0hnx.jpeg', 0, 'vans-LK4qB', 'Vans', 'Vans item', '2020-08-12 21:31:00', '2020-08-12 21:31:00'),
(36, 'Louis Vuitton', 'uploads/brands/4UU2dDySt1VTAm921sFX6o1LCm2vXyRv09Xq35Ca.jpeg', 0, 'Louis-Vuitton-kLOte', 'Louis Vuitton', 'Louis Vuitton items', '2020-08-12 21:32:22', '2020-08-12 21:32:22'),
(37, 'Under Armour', 'uploads/brands/JUpvFzhZ6fyhVVpHDW0eA8g0ucWWW2kSwMVQVoVB.jpeg', 0, 'Under-Armour-T2ON3', 'Under Armour', 'Under Armour items', '2020-08-12 21:33:12', '2020-08-12 21:33:12'),
(38, 'Mizuno', 'uploads/brands/5rUFr8hXsBjiwJgUEBAKzMndwd71DyFfc0MZf2SE.jpeg', 0, 'Mizuno-MX1fg', 'Mizuno', 'Mizuno Items', '2020-08-12 21:34:06', '2020-08-12 21:34:06'),
(39, 'Fila', 'uploads/brands/Gkwqmx60DawshCeNVNMXsr2YBlDhecFqeBK6y692.jpeg', 0, 'Fila-7ol9b', 'Fila', 'Fila items', '2020-08-12 21:36:29', '2020-08-12 21:36:29'),
(40, 'Champion', 'uploads/brands/jhzxI4naz2cxD2wSrKlomZb53WMY0lavHR0adQjw.jpeg', 0, 'Champion-dHatF', 'Champion', 'Champion items', '2020-08-12 21:37:16', '2020-08-12 21:37:16'),
(41, 'ADIDAS', 'uploads/brands/M5hwWeo36NAv2KoWuF5xxcfOAwzUaMSrQucRfnWU.jpeg', 0, 'ADIDAS-UD1Jw', 'ADIDAS', 'ADIDAS items', '2020-08-12 21:37:51', '2020-08-12 21:37:51'),
(42, 'Gucci', 'uploads/brands/yLrrjtCGn1xLPL1Asing1ZnnFupE8CwE3WSyraS9.jpeg', 0, 'Gucci-IuBfp', 'Gucci', 'Gucci items', '2020-08-12 21:38:24', '2020-08-12 21:38:24'),
(43, 'Levis', 'uploads/brands/Lr1IwtKKM5C8scbZVeZPpr28GpUT7B6EfAf7wicd.jpeg', 0, 'Levis-4H4za', 'levis', 'Levis item', '2020-08-12 21:39:29', '2020-08-12 21:39:29'),
(44, 'Erke', 'uploads/brands/h6EZj8QSP0BAoHVCqAk4HUvr3ftPHtUY5RTVfSK9.jpeg', 0, 'Erke-PVS8L', 'Erke', 'Erke items', '2020-08-12 21:39:51', '2020-08-12 21:39:51'),
(45, 'Nike', 'uploads/brands/Z9T4QfewgdqKfYDjf3eQsaCouYxcIreTQjxGpgeO.jpeg', 0, 'Nike-4GmxS', 'Nike', 'Nike items', '2020-08-12 21:40:24', '2020-08-12 21:40:24'),
(46, 'Puma', 'uploads/brands/rs7ZTlTWmKZWnwtgkTgsEc5DleAiHtMNOtsEtbkU.jpeg', 0, 'Puma-23W0u', 'Puma', 'Puma items', '2020-08-12 21:40:57', '2020-08-12 21:40:57'),
(47, 'Converse', 'uploads/brands/vrCXWlWvZf4hs2d2AliqxFd4vBS61VlASxk7IvsZ.jpeg', 0, 'Converse-kDKjh', 'Converse', 'Converse item', '2020-08-12 21:41:44', '2020-08-12 21:41:44'),
(48, 'Lee', 'uploads/brands/bipBZecPGxHWJar87y1gErOXqJ11XQUgnX7VgfcH.jpeg', 0, 'Lee-6Odh7', 'Lee', 'Lee items', '2020-08-12 21:42:05', '2020-08-12 21:42:23'),
(49, 'Boss', 'uploads/brands/IyLgDYQ3cXldB9KZn9bmvVOu19e8Er9tamlcjSsw.jpeg', 0, 'Boss-0DmZq', 'Boss', 'Boss items', '2020-08-12 21:43:25', '2020-08-12 21:43:25'),
(50, 'Lacoste', 'uploads/brands/qwxSb5yrWl3nQE5FBDvg6zA2NBCd01vnN5hSm78T.jpeg', 0, 'Lacoste-RMG93', 'Lacoste', 'Lacoste items', '2020-08-12 21:44:04', '2020-08-12 21:44:20'),
(51, 'Zara', 'uploads/brands/OXdhz2xWVnfthNzpAdOgSLZazPZTqEOn4mTQnIUz.jpeg', 0, 'Zara-1UFnK', 'Zara', 'Zara items', '2020-08-12 21:44:41', '2020-08-12 21:44:41'),
(52, 'Bata', 'uploads/brands/juqh4NvASczH0wZf87sjwp6dSyBXj7ATbIqiZRxg.jpeg', 0, 'Bata-lWRFZ', 'bata', 'Bata items', '2020-08-12 21:45:08', '2020-08-12 21:45:08'),
(53, 'Diesel', 'uploads/brands/hnGvqMkXIIqeE8j6LT9CkOKPoTREhOEunq0FSPV9.jpeg', 0, 'Diesel-fyDZI', 'Diesel', 'Diesel items', '2020-08-12 21:45:54', '2020-08-12 21:45:54'),
(54, 'Giorgio Armani', 'uploads/brands/anbhoPocDIUcT7g8MC1gtQ8c6jdlHZbypCln40g5.jpeg', 0, 'Giorgio-Armani-6LpLY', 'Giorgio Armani', 'Giorgio Armani items', '2020-08-12 21:47:05', '2020-08-12 21:47:05'),
(55, '361', 'uploads/brands/qTX8oNJrOg23NABn5aS2L5CIcj2KNrlhu3W3SEWR.jpeg', 0, '361-CXWJA', '361', '361 items', '2020-08-12 21:49:03', '2020-08-12 21:49:03'),
(56, 'Endura', 'uploads/brands/0yEqaq2yJfYkYxcnIbxPEU4zbYmp3pFe8HvLu66q.jpeg', 0, 'Endura-i6Zmc', 'Endura', 'Endura items', '2020-08-12 21:49:41', '2020-08-12 21:49:41'),
(57, 'Crizal', 'uploads/brands/WcL8vMKmyba3Gzs9WjgR9NTipe0PAZS32vl3rxlO.jpeg', 0, 'Crizal-2DFyx', 'Crizal', 'Crizal items', '2020-08-13 19:48:02', '2020-08-13 19:48:02'),
(58, 'Hero', 'uploads/brands/0gthuFz324txIT6IbE1kPtF29zsCZPSpSz5FEsyd.jpeg', 0, 'Hero-msLeZ', 'Hero', 'Hero items', '2020-08-13 20:05:18', '2020-08-13 20:05:18'),
(59, 'Timberland', 'uploads/brands/PN98YdOidHQJ3NJBUyyFsRvFAtHnaDk5p7Z8ISoo.jpeg', 0, 'Timberland-R4FHe', 'Timberland', 'Timberland products', '2020-08-13 20:05:53', '2020-08-13 20:05:53'),
(60, 'Benq', 'uploads/brands/o91ikVmOykl05jvWD0oPVCTycKWb0gYhceGSTR2b.jpeg', 0, 'Benq-JBkol', 'Benq', 'Benq products', '2020-08-13 20:06:21', '2020-08-13 20:06:21'),
(61, 'Dhikhar', 'uploads/brands/QfwCt5RSCsPYSErlFeHo9X5znm8L04TlW4jscLNB.jpeg', 0, 'Dhikhar-XLMHF', 'Shikhar', 'Shikhar products', '2020-08-13 20:06:53', '2020-08-13 20:06:53'),
(62, 'Seiko', 'uploads/brands/OgNfm2V9VtnOKoOZBSXBnVQS7JtwzIgwwT1RbowW.jpeg', 0, 'Seiko-RNGjJ', 'Seiko', 'Seiko products', '2020-08-13 20:07:18', '2020-08-13 20:07:32'),
(63, 'Rolex', 'uploads/brands/uC6o9Qa7UfHSE87eqG944pSrXTdL9x59jmo2sDZH.jpeg', 0, 'Rolex-rU6gV', 'Rolex', 'Rolex products', '2020-08-13 20:08:06', '2020-08-13 20:08:06'),
(64, 'Sonata', 'uploads/brands/wloi8sVDu6Nvsos78p7khNT4weaEvQz2bQg9FxtF.jpeg', 0, 'Sonata-pX1IZ', 'Sonata', 'Sonata products', '2020-08-13 20:08:39', '2020-08-13 20:08:39'),
(65, 'Hilife', 'uploads/brands/71Qg0COsfeskjJVxuIDWbcqe2w78l0oBLkhzDmPh.jpeg', 0, 'Hilife-oxmEu', 'Hilife', 'Hilife Product', '2020-08-13 20:09:14', '2020-08-13 20:09:14'),
(66, 'Goldstar', 'uploads/brands/rYmSAYYNJ6IZIzSNUzVF06jrVwvFN8Nb09cTU4mM.jpeg', 0, 'Goldstar-Qydyl', 'Goldstar', 'Goldstar product', '2020-08-13 20:11:16', '2020-08-13 20:11:16'),
(67, 'SMK Helmets', 'uploads/brands/0MG9zfJAYMeCjU9ace2t5di7K20PmyIBO2lMTG6c.jpeg', 0, 'SMK-Helmets-FC6ta', 'SMK Helmets', 'SMK Helmets PRODUCTS', '2020-08-13 20:11:59', '2020-08-13 20:11:59'),
(68, 'Royal', 'uploads/brands/raIGZvpAL25J3I4q7AK1f1PnesAhe5dSCQy8OKKL.jpeg', 0, 'Royal-fMYJQ', 'Royal', 'Royal products', '2020-08-13 20:13:02', '2020-08-13 20:13:02'),
(69, 'Godrej', 'uploads/brands/WhEx8JGEe5tGyQIINJANrEhUHVphaK8eTYPN2kHn.jpeg', 0, 'Godrej-KOudb', 'Godrej', 'Godrej product', '2020-08-13 20:13:36', '2020-08-13 20:13:36'),
(70, 'Raymond', 'uploads/brands/fvsSDT8aUrgLysHAblfvF9awb1pXR5RM43CtbFyQ.jpeg', 0, 'Raymond-KewAH', 'Raymond', 'Raymond product', '2020-08-13 20:14:25', '2020-08-13 20:14:25'),
(71, 'Vega', 'uploads/brands/UUJzD6Kp9l8FpoCYOBNMNu6kXxQ4smOQbaJkoSfT.jpeg', 0, 'Vega-OMod4', 'Vega', 'Vega products', '2020-08-13 20:15:02', '2020-08-13 20:15:02'),
(72, 'Arai', 'uploads/brands/hlapjaWjfM3eYr6zEhl2ICTv1mD5MZNYFmDdRyrV.jpeg', 0, 'Arai-quzRE', 'Arai', 'Arai products', '2020-08-13 20:15:38', '2020-08-13 20:15:38'),
(73, 'KTM CITY', 'uploads/brands/65A2gqjV85nHoJL0JkA5tbe0gqVkVO4eHzzS9yjj.jpeg', 0, 'KTM-CITY-yKu7q', 'KTM CITY', 'KTM CITY products', '2020-08-13 20:16:12', '2020-08-13 20:16:12'),
(74, 'Ray Ban', 'uploads/brands/GbCPupzWNXQ1eNueMpyon9jND2e5n2nRiwGtEPXN.jpeg', 0, 'Ray-Ban-8YgSP', 'Ray Ban', 'Ray Ban Products', '2020-08-13 20:16:37', '2020-08-13 20:16:37'),
(75, 'Hitachi', 'uploads/brands/RhRbFP6s0zrSpfto3AUo1suIa212k5dSgOWFY465.jpeg', 0, 'Hitachi-FD9GB', 'Hitachi', 'Hitachi products', '2020-08-13 20:17:02', '2020-08-13 20:17:02'),
(76, 'Yahama', 'uploads/brands/0eoAPrcYhlLnitOH1apCOe4sT721cFjR415TVVoj.jpeg', 0, 'Yahama-oUjQc', 'Yahama', 'Yahama Products', '2020-08-13 20:18:20', '2020-08-13 20:18:20'),
(77, 'Bajaj', 'uploads/brands/VTuIRIQ5ziHXepeW3WqtfBa0c9aj2nU9V0ntr1Mz.jpeg', 0, 'Bajaj-pZ80w', 'bajaj', 'Bajaj products', '2020-08-13 20:19:00', '2020-08-13 20:19:00'),
(78, 'wirlpool', 'uploads/brands/aeBUxg3hzyYxKdBDIsOil7g9N8UH17CyHSquLh9S.jpeg', 0, 'wirlpool-lWqO0', 'Wirlpool', 'Wirlpool Products', '2020-08-13 20:24:40', '2020-08-13 20:24:40'),
(79, 'Baltra', 'uploads/brands/vw8qCljjU8kzRYFLw62MAKCZlIdot4v6sQz4amO0.jpeg', 0, 'Baltra-mehxq', 'Baltra', 'baltra ptoducts', '2020-08-13 20:25:11', '2020-08-13 20:25:11'),
(80, 'Mammut', 'uploads/brands/3eHdGZtmqfC438cp5318FN9IZ7Ro2Kjr12D3XFqh.jpeg', 0, 'Mammut-qPfOS', 'Mammut', 'Mammut  product', '2020-08-13 20:26:47', '2020-08-13 20:26:47'),
(81, 'The North Face', 'uploads/brands/g3cuR5RXtPItbA2dgEjDchKQNbjztaPAv3HDM6XY.jpeg', 0, 'The-North-Face-G36qP', 'The North Face', 'The North Face', '2020-08-13 20:27:22', '2020-08-13 20:27:22'),
(82, 'Caliber', 'uploads/brands/E91DIdoX1b304yvaLjDfRgNdoYwzxAzDjsdWP44Z.jpeg', 0, 'Caliber-JwonN', 'Caliber', 'Caliber products', '2020-08-13 20:27:53', '2020-08-13 20:27:53');

-- --------------------------------------------------------

--
-- Table structure for table `business_settings`
--

CREATE TABLE `business_settings` (
  `id` int(11) NOT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `business_settings`
--

INSERT INTO `business_settings` (`id`, `type`, `value`, `created_at`, `updated_at`) VALUES
(1, 'home_default_currency', '1', '2018-10-16 01:35:52', '2019-01-28 01:26:53'),
(2, 'system_default_currency', '1', '2018-10-16 01:36:58', '2020-01-26 04:22:13'),
(3, 'currency_format', '1', '2018-10-17 03:01:59', '2018-10-17 03:01:59'),
(4, 'symbol_format', '1', '2018-10-17 03:01:59', '2019-01-20 02:10:55'),
(5, 'no_of_decimals', '3', '2018-10-17 03:01:59', '2020-03-04 00:57:16'),
(6, 'product_activation', '1', '2018-10-28 01:38:37', '2019-02-04 01:11:41'),
(7, 'vendor_system_activation', '1', '2018-10-28 07:44:16', '2019-02-04 01:11:38'),
(8, 'show_vendors', '1', '2018-10-28 07:44:47', '2019-02-04 01:11:13'),
(9, 'paypal_payment', '0', '2018-10-28 07:45:16', '2019-01-31 05:09:10'),
(10, 'stripe_payment', '0', '2018-10-28 07:45:47', '2018-11-14 01:51:51'),
(11, 'cash_payment', '1', '2018-10-28 07:46:05', '2019-01-24 03:40:18'),
(12, 'payumoney_payment', '0', '2018-10-28 07:46:27', '2019-03-05 05:41:36'),
(13, 'best_selling', '1', '2018-12-24 08:13:44', '2019-02-14 05:29:13'),
(14, 'paypal_sandbox', '0', '2019-01-16 12:44:18', '2019-01-16 12:44:18'),
(15, 'sslcommerz_sandbox', '1', '2019-01-16 12:44:18', '2019-03-14 00:07:26'),
(16, 'sslcommerz_payment', '0', '2019-01-24 09:39:07', '2019-01-29 06:13:46'),
(17, 'vendor_commission', '20', '2019-01-31 06:18:04', '2019-04-13 06:49:26'),
(18, 'verification_form', '[{\"type\":\"text\",\"label\":\"Your name\"},{\"type\":\"text\",\"label\":\"Shop name\"},{\"type\":\"text\",\"label\":\"Email\"},{\"type\":\"text\",\"label\":\"License No\"},{\"type\":\"text\",\"label\":\"Full Address\"},{\"type\":\"text\",\"label\":\"Phone Number\"},{\"type\":\"file\",\"label\":\"Tax Papers\"}]', '2019-02-03 11:36:58', '2019-02-16 06:14:42'),
(19, 'google_analytics', '0', '2019-02-06 12:22:35', '2019-02-06 12:22:35'),
(20, 'facebook_login', '0', '2019-02-07 12:51:59', '2019-02-08 19:41:15'),
(21, 'google_login', '0', '2019-02-07 12:52:10', '2019-02-08 19:41:14'),
(22, 'twitter_login', '0', '2019-02-07 12:52:20', '2019-02-08 02:32:56'),
(23, 'payumoney_payment', '1', '2019-03-05 11:38:17', '2019-03-05 11:38:17'),
(24, 'payumoney_sandbox', '1', '2019-03-05 11:38:17', '2019-03-05 05:39:18'),
(36, 'facebook_chat', '0', '2019-04-15 11:45:04', '2019-04-15 11:45:04'),
(37, 'email_verification', '0', '2019-04-30 07:30:07', '2019-04-30 07:30:07'),
(38, 'wallet_system', '0', '2019-05-19 08:05:44', '2019-05-19 02:11:57'),
(39, 'coupon_system', '0', '2019-06-11 09:46:18', '2019-06-11 09:46:18'),
(40, 'current_version', '3.1', '2019-06-11 09:46:18', '2019-06-11 09:46:18'),
(41, 'instamojo_payment', '0', '2019-07-06 09:58:03', '2019-07-06 09:58:03'),
(42, 'instamojo_sandbox', '1', '2019-07-06 09:58:43', '2019-07-06 09:58:43'),
(43, 'razorpay', '0', '2019-07-06 09:58:43', '2019-07-06 09:58:43'),
(44, 'paystack', '0', '2019-07-21 13:00:38', '2019-07-21 13:00:38'),
(45, 'pickup_point', '0', '2019-10-17 11:50:39', '2019-10-17 11:50:39'),
(46, 'maintenance_mode', '0', '2019-10-17 11:51:04', '2019-10-17 11:51:04'),
(47, 'voguepay', '0', '2019-10-17 11:51:24', '2019-10-17 11:51:24'),
(48, 'voguepay_sandbox', '0', '2019-10-17 11:51:38', '2019-10-17 11:51:38'),
(50, 'category_wise_commission', '0', '2020-01-21 07:22:47', '2020-01-21 07:22:47'),
(51, 'conversation_system', '1', '2020-01-21 07:23:21', '2020-01-21 07:23:21'),
(52, 'guest_checkout_active', '1', '2020-01-22 07:36:38', '2020-01-22 07:36:38'),
(53, 'facebook_pixel', '0', '2020-01-22 11:43:58', '2020-01-22 11:43:58'),
(55, 'classified_product', '0', '2020-05-13 13:01:05', '2020-05-13 13:01:05'),
(56, 'pos_activation_for_seller', '1', '2020-06-11 09:45:02', '2020-06-11 09:45:02'),
(57, 'shipping_type', 'product_wise_shipping', '2020-07-01 13:49:56', '2020-07-01 13:49:56'),
(58, 'flat_rate_shipping_cost', '0', '2020-07-01 13:49:56', '2020-07-01 13:49:56'),
(59, 'shipping_cost_admin', '0', '2020-07-01 13:49:56', '2020-07-01 13:49:56'),
(60, 'payhere_sandbox', '0', '2020-07-30 18:23:53', '2020-07-30 18:23:53'),
(61, 'payhere', '0', '2020-07-30 18:23:53', '2020-07-30 18:23:53');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `variation` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `tax` double(8,2) DEFAULT NULL,
  `shipping_cost` double(8,2) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `commision_rate` double(8,2) NOT NULL DEFAULT 0.00,
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured` int(1) NOT NULL DEFAULT 0,
  `top` int(1) NOT NULL DEFAULT 0,
  `digital` int(1) NOT NULL DEFAULT 0,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute_id` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `commision_rate`, `banner`, `icon`, `featured`, `top`, `digital`, `slug`, `meta_title`, `meta_description`, `attribute_id`, `created_at`, `updated_at`) VALUES
(1, 'Women\'s Fashion', 0.00, 'uploads/categories/banner/zCRBQMfpCCSwaCaRC7exgNS1ziJp053WoRqqqej4.jpeg', 'uploads/categories/icon/fhn0YNWAPiioEkzqQjzlkUbf3AfJQHojXx0O0oLT.jpeg', 1, 1, 0, 'womensfashion', 'Women\'s Fashion', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porttitor vitae nibh ac consectetur. Nam eu accumsan sem, id fringilla enim. Nulla non orci a felis molestie posuere. Donec nisl mauris, luctus sit amet velit vel, ullamcorper vehicula risus. Duis porta ac nunc eu porta. Praesent gravida ipsum quis lorem convallis, vitae congue magna vestibulum. Pellentesque hendrerit massa magna, quis vestibulum magna dapibus eget. Aenean eget orci tincidunt, feugiat nisl nec, pharetra arcu. Etiam justo orci, molestie vel augue in, tincidunt tempor elit. Donec ut vestibulum dolor, a sagittis sem. Nam nulla felis, porta a ligula volutpat, maximus rhoncus augue. Duis sed sodales lacus. Mauris elit libero, consectetur ut purus id, laoreet pharetra nibh.', '1,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40', '2020-08-13 10:09:20', '2020-08-13 21:54:20'),
(2, 'Men\'s Fashion', 0.00, 'uploads/categories/banner/Jf1QNGIQbnz2gE7AmXqiuzwlzeakmepSYZ2sS5qR.jpeg', 'uploads/categories/icon/7OVGatp0f4S5xRS30HPclNtucAlBGgTfN6z6uV0q.jpeg', 1, 1, 0, 'mensfashion', 'Men\'s Fashion', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porttitor vitae nibh ac consectetur. Nam eu accumsan sem, id fringilla enim. Nulla non orci a felis molestie posuere. Donec nisl mauris, luctus sit amet velit vel, ullamcorper vehicula risus. Duis porta ac nunc eu porta. Praesent gravida ipsum quis lorem convallis, vitae congue magna vestibulum. Pellentesque hendrerit massa magna, quis vestibulum magna dapibus eget. Aenean eget orci tincidunt, feugiat nisl nec, pharetra arcu. Etiam justo orci, molestie vel augue in, tincidunt tempor elit. Donec ut vestibulum dolor, a sagittis sem. Nam nulla felis, porta a ligula volutpat, maximus rhoncus augue. Duis sed sodales lacus. Mauris elit libero, consectetur ut purus id, laoreet pharetra nibh.', '1,2', '2020-08-13 10:09:05', '2020-08-13 21:54:05'),
(3, 'Electronic Devices', 0.00, 'uploads/categories/banner/zqVsHdSLpWLm3Z5nNHgoLVC40NQ82H11OYBt2T7V.jpeg', 'uploads/categories/icon/F3fVT7UVATcasR80XZ4u0wM5THTRpX06kK1XW58Y.jpeg', 1, 1, 0, 'electronicdevices', 'Electronic Devices', NULL, '1,4,6,7', '2020-08-13 10:08:43', '2020-08-13 21:53:43'),
(4, 'TV & Home Appliances', 0.00, 'uploads/categories/banner/JsH2c4NZvi2x4pKtk6AvAFumT5HTFrMJhzF9Jzmw.jpeg', 'uploads/categories/icon/SeKpeNZW3jGoOtg3KdUF6TMtIS8Q2NYV5Nj2bzJm.jpeg', 1, 1, 0, 'TV--Home-Appliances-PGzMq', 'Electronic Devices', 'TV & Home Appliances', '1,4,7', '2020-08-13 10:20:14', '2020-08-13 22:05:14'),
(5, 'Health & Beauty', 0.00, 'uploads/categories/banner/SO2q7w0cEcClv2BDtJ6BWYwvqPRemi9OjlR4XRGy.jpeg', 'uploads/categories/icon/PGCop8RgYBhwlfrSDDNKn87zDxQNepz3ECeMr2hV.jpeg', 1, 1, 0, 'Health--Beauty-oBMvU', 'Health&Beauty', 'Health & Beauty', '1,5,6,8', '2020-08-13 09:57:33', '2020-08-13 21:42:33'),
(6, 'Babies & Toys', 0.00, 'uploads/categories/banner/tyeGw4444rd6DG4WmEvWdc80tJdmqJkHt3TTlE8D.jpeg', 'uploads/categories/icon/2nIzjJc6my1hXQEaot7LiH0ddWPS8mD6IB5Lb3ZD.jpeg', 1, 1, 0, 'Babies--Toys-FMz8t', 'Babies&Toys', 'Babies & Toys', '1,2,6,8', '2020-08-13 10:08:19', '2020-08-13 21:53:19'),
(7, 'Groceries & Pets', 0.00, 'uploads/categories/banner/SpRWXeaaZAFiHFBa2yYMAyRgKrOzDsBAIApxgVxi.jpeg', 'uploads/categories/icon/zfNaPRE4QPBItqxJIedfqrnBGSBF6XKZrzLEM1SJ.jpeg', 1, 0, 0, 'Groceries--Pets-JAvge', 'Groceries&Pets', 'Groceries & Pets', '5,6,8', '2020-08-13 10:19:58', '2020-08-13 22:04:58'),
(8, 'Home & Lifestyle', 0.00, 'uploads/categories/banner/6Pi7xUUBK3cKryy2kMUnZfPrnIpBjWVfXsgqF7at.jpeg', 'uploads/categories/icon/O9tY1khSCK2rPX4VLYHySICjsCHGTOjfQNEQdRoV.jpeg', 1, 0, 0, 'Home--Lifestyle-XT7Tc', 'Home&Lifestyle', 'Home & Lifestyle', '1,2,5', '2020-08-13 10:02:09', '2020-08-13 21:47:09'),
(9, 'Watches & Accessories', 0.00, 'uploads/categories/banner/JeuHSEOL0YC1n9xt9HZhD5LHIEl9N4aWmpASMls7.jpeg', 'uploads/categories/icon/5zfcWT5aSO4LSuqA627zR9Fl6lLQ1QpKalU6Lh7w.jpeg', 1, 0, 0, 'Watches--Accessories-Nj1cg', 'Watches & Accessories', 'Watches & Accessories', '1,6', '2020-08-13 09:57:17', '2020-08-13 21:42:17'),
(10, 'Sports & Outdoor', 0.00, 'uploads/categories/banner/XkaT0TvQKlvnqNvJapIdrULZ701cl0QnasdESr8U.jpeg', 'uploads/categories/icon/jvPmFJPK2qAkbrnp6Skn29hYeA4RkxKAaCnME3b3.jpeg', 1, 0, 0, 'Sports--Outdoor-Sajmt', 'Sports & Outdoor', 'Sports & Outdoor', '1,5,6,8', '2020-08-13 10:29:16', '2020-08-13 22:14:16'),
(11, 'Automotive & Motorbike', 0.00, 'uploads/categories/banner/EWYZMWK9OVQDRb9FtEsDUCbqzh5zoYeq9UutrlBQ.jpeg', 'uploads/categories/icon/shXU11MmkWSe6kfXHwDQAWY3Eg1mVqjs7vt3HYS8.jpeg', 1, 0, 0, 'Automotive--Motorbike-dq5hj', 'Automotive & Motorbike', 'Automotive & Motorbike', '1,6,8', '2020-08-13 09:39:37', '2020-08-13 21:24:37'),
(12, 'Electronic Accessories', 0.00, 'uploads/categories/banner/dlM0oFPuQ0p1MeTLj7b2dWg67DEm4TkRTiSDGePf.jpeg', 'uploads/categories/icon/LZ5C871Gsm5gazYe0hnLBNUo2SUhKCdQj74DU350.jpeg', 1, 0, 0, 'Electronic-Accessories-Bg2SS', 'Electronic Accessories', 'All Electronic Accessories', '4,7,9', '2020-08-13 10:18:58', '2020-08-13 22:03:58');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'IndianRed', '#CD5C5C', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(2, 'LightCoral', '#F08080', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(3, 'Salmon', '#FA8072', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(4, 'DarkSalmon', '#E9967A', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(5, 'LightSalmon', '#FFA07A', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(6, 'Crimson', '#DC143C', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(7, 'Red', '#FF0000', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(8, 'FireBrick', '#B22222', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(9, 'DarkRed', '#8B0000', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(10, 'Pink', '#FFC0CB', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(11, 'LightPink', '#FFB6C1', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(12, 'HotPink', '#FF69B4', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(13, 'DeepPink', '#FF1493', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(14, 'MediumVioletRed', '#C71585', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(15, 'PaleVioletRed', '#DB7093', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(16, 'LightSalmon', '#FFA07A', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(17, 'Coral', '#FF7F50', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(18, 'Tomato', '#FF6347', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(19, 'OrangeRed', '#FF4500', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(20, 'DarkOrange', '#FF8C00', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(21, 'Orange', '#FFA500', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(22, 'Gold', '#FFD700', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(23, 'Yellow', '#FFFF00', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(24, 'LightYellow', '#FFFFE0', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(25, 'LemonChiffon', '#FFFACD', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(26, 'LightGoldenrodYellow', '#FAFAD2', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(27, 'PapayaWhip', '#FFEFD5', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(28, 'Moccasin', '#FFE4B5', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(29, 'PeachPuff', '#FFDAB9', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(30, 'PaleGoldenrod', '#EEE8AA', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(31, 'Khaki', '#F0E68C', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(32, 'DarkKhaki', '#BDB76B', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(33, 'Lavender', '#E6E6FA', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(34, 'Thistle', '#D8BFD8', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(35, 'Plum', '#DDA0DD', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(36, 'Violet', '#EE82EE', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(37, 'Orchid', '#DA70D6', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(38, 'Fuchsia', '#FF00FF', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(39, 'Magenta', '#FF00FF', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(40, 'MediumOrchid', '#BA55D3', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(41, 'MediumPurple', '#9370DB', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(42, 'Amethyst', '#9966CC', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(43, 'BlueViolet', '#8A2BE2', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(44, 'DarkViolet', '#9400D3', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(45, 'DarkOrchid', '#9932CC', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(46, 'DarkMagenta', '#8B008B', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(47, 'Purple', '#800080', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(48, 'Indigo', '#4B0082', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(49, 'SlateBlue', '#6A5ACD', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(50, 'DarkSlateBlue', '#483D8B', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(51, 'MediumSlateBlue', '#7B68EE', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(52, 'GreenYellow', '#ADFF2F', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(53, 'Chartreuse', '#7FFF00', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(54, 'LawnGreen', '#7CFC00', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(55, 'Lime', '#00FF00', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(56, 'LimeGreen', '#32CD32', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(57, 'PaleGreen', '#98FB98', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(58, 'LightGreen', '#90EE90', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(59, 'MediumSpringGreen', '#00FA9A', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(60, 'SpringGreen', '#00FF7F', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(61, 'MediumSeaGreen', '#3CB371', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(62, 'SeaGreen', '#2E8B57', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(63, 'ForestGreen', '#228B22', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(64, 'Green', '#008000', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(65, 'DarkGreen', '#006400', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(66, 'YellowGreen', '#9ACD32', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(67, 'OliveDrab', '#6B8E23', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(68, 'Olive', '#808000', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(69, 'DarkOliveGreen', '#556B2F', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(70, 'MediumAquamarine', '#66CDAA', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(71, 'DarkSeaGreen', '#8FBC8F', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(72, 'LightSeaGreen', '#20B2AA', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(73, 'DarkCyan', '#008B8B', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(74, 'Teal', '#008080', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(75, 'Aqua', '#00FFFF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(76, 'Cyan', '#00FFFF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(77, 'LightCyan', '#E0FFFF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(78, 'PaleTurquoise', '#AFEEEE', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(79, 'Aquamarine', '#7FFFD4', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(80, 'Turquoise', '#40E0D0', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(81, 'MediumTurquoise', '#48D1CC', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(82, 'DarkTurquoise', '#00CED1', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(83, 'CadetBlue', '#5F9EA0', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(84, 'SteelBlue', '#4682B4', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(85, 'LightSteelBlue', '#B0C4DE', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(86, 'PowderBlue', '#B0E0E6', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(87, 'LightBlue', '#ADD8E6', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(88, 'SkyBlue', '#87CEEB', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(89, 'LightSkyBlue', '#87CEFA', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(90, 'DeepSkyBlue', '#00BFFF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(91, 'DodgerBlue', '#1E90FF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(92, 'CornflowerBlue', '#6495ED', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(93, 'MediumSlateBlue', '#7B68EE', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(94, 'RoyalBlue', '#4169E1', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(95, 'Blue', '#0000FF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(96, 'MediumBlue', '#0000CD', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(97, 'DarkBlue', '#00008B', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(98, 'Navy', '#000080', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(99, 'MidnightBlue', '#191970', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(100, 'Cornsilk', '#FFF8DC', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(101, 'BlanchedAlmond', '#FFEBCD', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(102, 'Bisque', '#FFE4C4', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(103, 'NavajoWhite', '#FFDEAD', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(104, 'Wheat', '#F5DEB3', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(105, 'BurlyWood', '#DEB887', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(106, 'Tan', '#D2B48C', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(107, 'RosyBrown', '#BC8F8F', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(108, 'SandyBrown', '#F4A460', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(109, 'Goldenrod', '#DAA520', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(110, 'DarkGoldenrod', '#B8860B', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(111, 'Peru', '#CD853F', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(112, 'Chocolate', '#D2691E', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(113, 'SaddleBrown', '#8B4513', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(114, 'Sienna', '#A0522D', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(115, 'Brown', '#A52A2A', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(116, 'Maroon', '#800000', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(117, 'White', '#FFFFFF', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(118, 'Snow', '#FFFAFA', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(119, 'Honeydew', '#F0FFF0', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(120, 'MintCream', '#F5FFFA', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(121, 'Azure', '#F0FFFF', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(122, 'AliceBlue', '#F0F8FF', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(123, 'GhostWhite', '#F8F8FF', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(124, 'WhiteSmoke', '#F5F5F5', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(125, 'Seashell', '#FFF5EE', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(126, 'Beige', '#F5F5DC', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(127, 'OldLace', '#FDF5E6', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(128, 'FloralWhite', '#FFFAF0', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(129, 'Ivory', '#FFFFF0', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(130, 'AntiqueWhite', '#FAEBD7', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(131, 'Linen', '#FAF0E6', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(132, 'LavenderBlush', '#FFF0F5', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(133, 'MistyRose', '#FFE4E1', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(134, 'Gainsboro', '#DCDCDC', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(135, 'LightGrey', '#D3D3D3', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(136, 'Silver', '#C0C0C0', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(137, 'DarkGray', '#A9A9A9', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(138, 'Gray', '#808080', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(139, 'DimGray', '#696969', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(140, 'LightSlateGray', '#778899', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(141, 'SlateGray', '#708090', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(142, 'DarkSlateGray', '#2F4F4F', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(143, 'Black', '#000000', '2018-11-05 02:12:30', '2018-11-05 02:12:30');

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `title` varchar(1000) COLLATE utf32_unicode_ci DEFAULT NULL,
  `sender_viewed` int(1) NOT NULL DEFAULT 1,
  `receiver_viewed` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `conversations`
--

INSERT INTO `conversations` (`id`, `sender_id`, `receiver_id`, `title`, `sender_viewed`, `receiver_viewed`, `created_at`, `updated_at`) VALUES
(2, 20, 21, 'Brown Lace-Up Glossy Formal Shoes For Men (G-917)', 1, 1, '2020-07-17 08:07:42', '2020-07-17 08:08:01'),
(3, 20, 21, 'Brown Lace-Up Glossy Formal Shoes For Men (G-917)', 1, 1, '2020-07-17 08:07:43', '2020-07-17 08:08:31'),
(4, 12, 21, 'hello product', 1, 1, '2020-08-05 01:28:38', '2020-08-12 05:39:02');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'AF', 'Afghanistan', 1, NULL, NULL),
(2, 'AL', 'Albania', 1, NULL, NULL),
(3, 'DZ', 'Algeria', 1, NULL, NULL),
(4, 'DS', 'American Samoa', 1, NULL, NULL),
(5, 'AD', 'Andorra', 1, NULL, NULL),
(6, 'AO', 'Angola', 1, NULL, NULL),
(7, 'AI', 'Anguilla', 1, NULL, NULL),
(8, 'AQ', 'Antarctica', 1, NULL, NULL),
(9, 'AG', 'Antigua and Barbuda', 1, NULL, NULL),
(10, 'AR', 'Argentina', 1, NULL, NULL),
(11, 'AM', 'Armenia', 1, NULL, NULL),
(12, 'AW', 'Aruba', 1, NULL, NULL),
(13, 'AU', 'Australia', 1, NULL, NULL),
(14, 'AT', 'Austria', 1, NULL, NULL),
(15, 'AZ', 'Azerbaijan', 1, NULL, NULL),
(16, 'BS', 'Bahamas', 1, NULL, NULL),
(17, 'BH', 'Bahrain', 1, NULL, NULL),
(18, 'BD', 'Bangladesh', 1, NULL, NULL),
(19, 'BB', 'Barbados', 1, NULL, NULL),
(20, 'BY', 'Belarus', 1, NULL, NULL),
(21, 'BE', 'Belgium', 1, NULL, NULL),
(22, 'BZ', 'Belize', 1, NULL, NULL),
(23, 'BJ', 'Benin', 1, NULL, NULL),
(24, 'BM', 'Bermuda', 1, NULL, NULL),
(25, 'BT', 'Bhutan', 1, NULL, NULL),
(26, 'BO', 'Bolivia', 1, NULL, NULL),
(27, 'BA', 'Bosnia and Herzegovina', 1, NULL, NULL),
(28, 'BW', 'Botswana', 1, NULL, NULL),
(29, 'BV', 'Bouvet Island', 1, NULL, NULL),
(30, 'BR', 'Brazil', 1, NULL, NULL),
(31, 'IO', 'British Indian Ocean Territory', 1, NULL, NULL),
(32, 'BN', 'Brunei Darussalam', 1, NULL, NULL),
(33, 'BG', 'Bulgaria', 1, NULL, NULL),
(34, 'BF', 'Burkina Faso', 1, NULL, NULL),
(35, 'BI', 'Burundi', 1, NULL, NULL),
(36, 'KH', 'Cambodia', 1, NULL, NULL),
(37, 'CM', 'Cameroon', 1, NULL, NULL),
(38, 'CA', 'Canada', 1, NULL, NULL),
(39, 'CV', 'Cape Verde', 1, NULL, NULL),
(40, 'KY', 'Cayman Islands', 1, NULL, NULL),
(41, 'CF', 'Central African Republic', 1, NULL, NULL),
(42, 'TD', 'Chad', 1, NULL, NULL),
(43, 'CL', 'Chile', 1, NULL, NULL),
(44, 'CN', 'China', 1, NULL, NULL),
(45, 'CX', 'Christmas Island', 1, NULL, NULL),
(46, 'CC', 'Cocos (Keeling) Islands', 1, NULL, NULL),
(47, 'CO', 'Colombia', 1, NULL, NULL),
(48, 'KM', 'Comoros', 1, NULL, NULL),
(49, 'CG', 'Congo', 1, NULL, NULL),
(50, 'CK', 'Cook Islands', 1, NULL, NULL),
(51, 'CR', 'Costa Rica', 1, NULL, NULL),
(52, 'HR', 'Croatia (Hrvatska)', 1, NULL, NULL),
(53, 'CU', 'Cuba', 1, NULL, NULL),
(54, 'CY', 'Cyprus', 1, NULL, NULL),
(55, 'CZ', 'Czech Republic', 1, NULL, NULL),
(56, 'DK', 'Denmark', 1, NULL, NULL),
(57, 'DJ', 'Djibouti', 1, NULL, NULL),
(58, 'DM', 'Dominica', 1, NULL, NULL),
(59, 'DO', 'Dominican Republic', 1, NULL, NULL),
(60, 'TP', 'East Timor', 1, NULL, NULL),
(61, 'EC', 'Ecuador', 1, NULL, NULL),
(62, 'EG', 'Egypt', 1, NULL, NULL),
(63, 'SV', 'El Salvador', 1, NULL, NULL),
(64, 'GQ', 'Equatorial Guinea', 1, NULL, NULL),
(65, 'ER', 'Eritrea', 1, NULL, NULL),
(66, 'EE', 'Estonia', 1, NULL, NULL),
(67, 'ET', 'Ethiopia', 1, NULL, NULL),
(68, 'FK', 'Falkland Islands (Malvinas)', 1, NULL, NULL),
(69, 'FO', 'Faroe Islands', 1, NULL, NULL),
(70, 'FJ', 'Fiji', 1, NULL, NULL),
(71, 'FI', 'Finland', 1, NULL, NULL),
(72, 'FR', 'France', 1, NULL, NULL),
(73, 'FX', 'France, Metropolitan', 1, NULL, NULL),
(74, 'GF', 'French Guiana', 1, NULL, NULL),
(75, 'PF', 'French Polynesia', 1, NULL, NULL),
(76, 'TF', 'French Southern Territories', 1, NULL, NULL),
(77, 'GA', 'Gabon', 1, NULL, NULL),
(78, 'GM', 'Gambia', 1, NULL, NULL),
(79, 'GE', 'Georgia', 1, NULL, NULL),
(80, 'DE', 'Germany', 1, NULL, NULL),
(81, 'GH', 'Ghana', 1, NULL, NULL),
(82, 'GI', 'Gibraltar', 1, NULL, NULL),
(83, 'GK', 'Guernsey', 1, NULL, NULL),
(84, 'GR', 'Greece', 1, NULL, NULL),
(85, 'GL', 'Greenland', 1, NULL, NULL),
(86, 'GD', 'Grenada', 1, NULL, NULL),
(87, 'GP', 'Guadeloupe', 1, NULL, NULL),
(88, 'GU', 'Guam', 1, NULL, NULL),
(89, 'GT', 'Guatemala', 1, NULL, NULL),
(90, 'GN', 'Guinea', 1, NULL, NULL),
(91, 'GW', 'Guinea-Bissau', 1, NULL, NULL),
(92, 'GY', 'Guyana', 1, NULL, NULL),
(93, 'HT', 'Haiti', 1, NULL, NULL),
(94, 'HM', 'Heard and Mc Donald Islands', 1, NULL, NULL),
(95, 'HN', 'Honduras', 1, NULL, NULL),
(96, 'HK', 'Hong Kong', 1, NULL, NULL),
(97, 'HU', 'Hungary', 1, NULL, NULL),
(98, 'IS', 'Iceland', 1, NULL, NULL),
(99, 'IN', 'India', 1, NULL, NULL),
(100, 'IM', 'Isle of Man', 1, NULL, NULL),
(101, 'ID', 'Indonesia', 1, NULL, NULL),
(102, 'IR', 'Iran (Islamic Republic of)', 1, NULL, NULL),
(103, 'IQ', 'Iraq', 1, NULL, NULL),
(104, 'IE', 'Ireland', 1, NULL, NULL),
(105, 'IL', 'Israel', 1, NULL, NULL),
(106, 'IT', 'Italy', 1, NULL, NULL),
(107, 'CI', 'Ivory Coast', 1, NULL, NULL),
(108, 'JE', 'Jersey', 1, NULL, NULL),
(109, 'JM', 'Jamaica', 1, NULL, NULL),
(110, 'JP', 'Japan', 1, NULL, NULL),
(111, 'JO', 'Jordan', 1, NULL, NULL),
(112, 'KZ', 'Kazakhstan', 1, NULL, NULL),
(113, 'KE', 'Kenya', 1, NULL, NULL),
(114, 'KI', 'Kiribati', 1, NULL, NULL),
(115, 'KP', 'Korea, Democratic People\'s Republic of', 1, NULL, NULL),
(116, 'KR', 'Korea, Republic of', 1, NULL, NULL),
(117, 'XK', 'Kosovo', 1, NULL, NULL),
(118, 'KW', 'Kuwait', 1, NULL, NULL),
(119, 'KG', 'Kyrgyzstan', 1, NULL, NULL),
(120, 'LA', 'Lao People\'s Democratic Republic', 1, NULL, NULL),
(121, 'LV', 'Latvia', 1, NULL, NULL),
(122, 'LB', 'Lebanon', 1, NULL, NULL),
(123, 'LS', 'Lesotho', 1, NULL, NULL),
(124, 'LR', 'Liberia', 1, NULL, NULL),
(125, 'LY', 'Libyan Arab Jamahiriya', 1, NULL, NULL),
(126, 'LI', 'Liechtenstein', 1, NULL, NULL),
(127, 'LT', 'Lithuania', 1, NULL, NULL),
(128, 'LU', 'Luxembourg', 1, NULL, NULL),
(129, 'MO', 'Macau', 1, NULL, NULL),
(130, 'MK', 'Macedonia', 1, NULL, NULL),
(131, 'MG', 'Madagascar', 1, NULL, NULL),
(132, 'MW', 'Malawi', 1, NULL, NULL),
(133, 'MY', 'Malaysia', 1, NULL, NULL),
(134, 'MV', 'Maldives', 1, NULL, NULL),
(135, 'ML', 'Mali', 1, NULL, NULL),
(136, 'MT', 'Malta', 1, NULL, NULL),
(137, 'MH', 'Marshall Islands', 1, NULL, NULL),
(138, 'MQ', 'Martinique', 1, NULL, NULL),
(139, 'MR', 'Mauritania', 1, NULL, NULL),
(140, 'MU', 'Mauritius', 1, NULL, NULL),
(141, 'TY', 'Mayotte', 1, NULL, NULL),
(142, 'MX', 'Mexico', 1, NULL, NULL),
(143, 'FM', 'Micronesia, Federated States of', 1, NULL, NULL),
(144, 'MD', 'Moldova, Republic of', 1, NULL, NULL),
(145, 'MC', 'Monaco', 1, NULL, NULL),
(146, 'MN', 'Mongolia', 1, NULL, NULL),
(147, 'ME', 'Montenegro', 1, NULL, NULL),
(148, 'MS', 'Montserrat', 1, NULL, NULL),
(149, 'MA', 'Morocco', 1, NULL, NULL),
(150, 'MZ', 'Mozambique', 1, NULL, NULL),
(151, 'MM', 'Myanmar', 1, NULL, NULL),
(152, 'NA', 'Namibia', 1, NULL, NULL),
(153, 'NR', 'Nauru', 1, NULL, NULL),
(154, 'NP', 'Nepal', 1, NULL, NULL),
(155, 'NL', 'Netherlands', 1, NULL, NULL),
(156, 'AN', 'Netherlands Antilles', 1, NULL, NULL),
(157, 'NC', 'New Caledonia', 1, NULL, NULL),
(158, 'NZ', 'New Zealand', 1, NULL, NULL),
(159, 'NI', 'Nicaragua', 1, NULL, NULL),
(160, 'NE', 'Niger', 1, NULL, NULL),
(161, 'NG', 'Nigeria', 1, NULL, NULL),
(162, 'NU', 'Niue', 1, NULL, NULL),
(163, 'NF', 'Norfolk Island', 1, NULL, NULL),
(164, 'MP', 'Northern Mariana Islands', 1, NULL, NULL),
(165, 'NO', 'Norway', 1, NULL, NULL),
(166, 'OM', 'Oman', 1, NULL, NULL),
(167, 'PK', 'Pakistan', 1, NULL, NULL),
(168, 'PW', 'Palau', 1, NULL, NULL),
(169, 'PS', 'Palestine', 1, NULL, NULL),
(170, 'PA', 'Panama', 1, NULL, NULL),
(171, 'PG', 'Papua New Guinea', 1, NULL, NULL),
(172, 'PY', 'Paraguay', 1, NULL, NULL),
(173, 'PE', 'Peru', 1, NULL, NULL),
(174, 'PH', 'Philippines', 1, NULL, NULL),
(175, 'PN', 'Pitcairn', 1, NULL, NULL),
(176, 'PL', 'Poland', 1, NULL, NULL),
(177, 'PT', 'Portugal', 1, NULL, NULL),
(178, 'PR', 'Puerto Rico', 1, NULL, NULL),
(179, 'QA', 'Qatar', 1, NULL, NULL),
(180, 'RE', 'Reunion', 1, NULL, NULL),
(181, 'RO', 'Romania', 1, NULL, NULL),
(182, 'RU', 'Russian Federation', 1, NULL, NULL),
(183, 'RW', 'Rwanda', 1, NULL, NULL),
(184, 'KN', 'Saint Kitts and Nevis', 1, NULL, NULL),
(185, 'LC', 'Saint Lucia', 1, NULL, NULL),
(186, 'VC', 'Saint Vincent and the Grenadines', 1, NULL, NULL),
(187, 'WS', 'Samoa', 1, NULL, NULL),
(188, 'SM', 'San Marino', 1, NULL, NULL),
(189, 'ST', 'Sao Tome and Principe', 1, NULL, NULL),
(190, 'SA', 'Saudi Arabia', 1, NULL, NULL),
(191, 'SN', 'Senegal', 1, NULL, NULL),
(192, 'RS', 'Serbia', 1, NULL, NULL),
(193, 'SC', 'Seychelles', 1, NULL, NULL),
(194, 'SL', 'Sierra Leone', 1, NULL, NULL),
(195, 'SG', 'Singapore', 1, NULL, NULL),
(196, 'SK', 'Slovakia', 1, NULL, NULL),
(197, 'SI', 'Slovenia', 1, NULL, NULL),
(198, 'SB', 'Solomon Islands', 1, NULL, NULL),
(199, 'SO', 'Somalia', 1, NULL, NULL),
(200, 'ZA', 'South Africa', 1, NULL, NULL),
(201, 'GS', 'South Georgia South Sandwich Islands', 1, NULL, NULL),
(202, 'SS', 'South Sudan', 1, NULL, NULL),
(203, 'ES', 'Spain', 1, NULL, NULL),
(204, 'LK', 'Sri Lanka', 1, NULL, NULL),
(205, 'SH', 'St. Helena', 1, NULL, NULL),
(206, 'PM', 'St. Pierre and Miquelon', 1, NULL, NULL),
(207, 'SD', 'Sudan', 1, NULL, NULL),
(208, 'SR', 'Suriname', 1, NULL, NULL),
(209, 'SJ', 'Svalbard and Jan Mayen Islands', 1, NULL, NULL),
(210, 'SZ', 'Swaziland', 1, NULL, NULL),
(211, 'SE', 'Sweden', 1, NULL, NULL),
(212, 'CH', 'Switzerland', 1, NULL, NULL),
(213, 'SY', 'Syrian Arab Republic', 1, NULL, NULL),
(214, 'TW', 'Taiwan', 1, NULL, NULL),
(215, 'TJ', 'Tajikistan', 1, NULL, NULL),
(216, 'TZ', 'Tanzania, United Republic of', 1, NULL, NULL),
(217, 'TH', 'Thailand', 1, NULL, NULL),
(218, 'TG', 'Togo', 1, NULL, NULL),
(219, 'TK', 'Tokelau', 1, NULL, NULL),
(220, 'TO', 'Tonga', 1, NULL, NULL),
(221, 'TT', 'Trinidad and Tobago', 1, NULL, NULL),
(222, 'TN', 'Tunisia', 1, NULL, NULL),
(223, 'TR', 'Turkey', 1, NULL, NULL),
(224, 'TM', 'Turkmenistan', 1, NULL, NULL),
(225, 'TC', 'Turks and Caicos Islands', 1, NULL, NULL),
(226, 'TV', 'Tuvalu', 1, NULL, NULL),
(227, 'UG', 'Uganda', 1, NULL, NULL),
(228, 'UA', 'Ukraine', 1, NULL, NULL),
(229, 'AE', 'United Arab Emirates', 1, NULL, NULL),
(230, 'GB', 'United Kingdom', 1, NULL, NULL),
(231, 'US', 'United States', 1, NULL, NULL),
(232, 'UM', 'United States minor outlying islands', 1, NULL, NULL),
(233, 'UY', 'Uruguay', 1, NULL, NULL),
(234, 'UZ', 'Uzbekistan', 1, NULL, NULL),
(235, 'VU', 'Vanuatu', 1, NULL, NULL),
(236, 'VA', 'Vatican City State', 1, NULL, NULL),
(237, 'VE', 'Venezuela', 1, NULL, NULL),
(238, 'VN', 'Vietnam', 1, NULL, NULL),
(239, 'VG', 'Virgin Islands (British)', 1, NULL, NULL),
(240, 'VI', 'Virgin Islands (U.S.)', 1, NULL, NULL),
(241, 'WF', 'Wallis and Futuna Islands', 1, NULL, NULL),
(242, 'EH', 'Western Sahara', 1, NULL, NULL),
(243, 'YE', 'Yemen', 1, NULL, NULL),
(244, 'ZR', 'Zaire', 1, NULL, NULL),
(245, 'ZM', 'Zambia', 1, NULL, NULL),
(246, 'ZW', 'Zimbabwe', 1, NULL, NULL),
(247, 'AF', 'Afghanistan', 1, NULL, NULL),
(248, 'AL', 'Albania', 1, NULL, NULL),
(249, 'DZ', 'Algeria', 1, NULL, NULL),
(250, 'DS', 'American Samoa', 1, NULL, NULL),
(251, 'AD', 'Andorra', 1, NULL, NULL),
(252, 'AO', 'Angola', 1, NULL, NULL),
(253, 'AI', 'Anguilla', 1, NULL, NULL),
(254, 'AQ', 'Antarctica', 1, NULL, NULL),
(255, 'AG', 'Antigua and Barbuda', 1, NULL, NULL),
(256, 'AR', 'Argentina', 1, NULL, NULL),
(257, 'AM', 'Armenia', 1, NULL, NULL),
(258, 'AW', 'Aruba', 1, NULL, NULL),
(259, 'AU', 'Australia', 1, NULL, NULL),
(260, 'AT', 'Austria', 1, NULL, NULL),
(261, 'AZ', 'Azerbaijan', 1, NULL, NULL),
(262, 'BS', 'Bahamas', 1, NULL, NULL),
(263, 'BH', 'Bahrain', 1, NULL, NULL),
(264, 'BD', 'Bangladesh', 1, NULL, NULL),
(265, 'BB', 'Barbados', 1, NULL, NULL),
(266, 'BY', 'Belarus', 1, NULL, NULL),
(267, 'BE', 'Belgium', 1, NULL, NULL),
(268, 'BZ', 'Belize', 1, NULL, NULL),
(269, 'BJ', 'Benin', 1, NULL, NULL),
(270, 'BM', 'Bermuda', 1, NULL, NULL),
(271, 'BT', 'Bhutan', 1, NULL, NULL),
(272, 'BO', 'Bolivia', 1, NULL, NULL),
(273, 'BA', 'Bosnia and Herzegovina', 1, NULL, NULL),
(274, 'BW', 'Botswana', 1, NULL, NULL),
(275, 'BV', 'Bouvet Island', 1, NULL, NULL),
(276, 'BR', 'Brazil', 1, NULL, NULL),
(277, 'IO', 'British Indian Ocean Territory', 1, NULL, NULL),
(278, 'BN', 'Brunei Darussalam', 1, NULL, NULL),
(279, 'BG', 'Bulgaria', 1, NULL, NULL),
(280, 'BF', 'Burkina Faso', 1, NULL, NULL),
(281, 'BI', 'Burundi', 1, NULL, NULL),
(282, 'KH', 'Cambodia', 1, NULL, NULL),
(283, 'CM', 'Cameroon', 1, NULL, NULL),
(284, 'CA', 'Canada', 1, NULL, NULL),
(285, 'CV', 'Cape Verde', 1, NULL, NULL),
(286, 'KY', 'Cayman Islands', 1, NULL, NULL),
(287, 'CF', 'Central African Republic', 1, NULL, NULL),
(288, 'TD', 'Chad', 1, NULL, NULL),
(289, 'CL', 'Chile', 1, NULL, NULL),
(290, 'CN', 'China', 1, NULL, NULL),
(291, 'CX', 'Christmas Island', 1, NULL, NULL),
(292, 'CC', 'Cocos (Keeling) Islands', 1, NULL, NULL),
(293, 'CO', 'Colombia', 1, NULL, NULL),
(294, 'KM', 'Comoros', 1, NULL, NULL),
(295, 'CG', 'Congo', 1, NULL, NULL),
(296, 'CK', 'Cook Islands', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci NOT NULL,
  `discount` double(8,2) NOT NULL,
  `discount_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` int(15) NOT NULL,
  `end_date` int(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `type`, `code`, `details`, `discount`, `discount_type`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 'cart_base', 'HAPPY', '{\"min_buy\":\"1500\",\"max_discount\":\"250\"}', 10.00, 'percent', 1594512000, 1622419200, '2020-07-12 07:32:45', '2020-07-12 07:32:45');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_usages`
--

CREATE TABLE `coupon_usages` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `coupon_usages`
--

INSERT INTO `coupon_usages` (`id`, `user_id`, `coupon_id`, `created_at`, `updated_at`) VALUES
(1, 8, 1, '2020-07-27 06:38:17', '2020-07-27 06:38:17'),
(2, 8, 1, '2020-07-27 06:39:03', '2020-07-27 06:39:03');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `exchange_rate` double(10,5) NOT NULL,
  `status` int(10) NOT NULL DEFAULT 0,
  `code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `symbol`, `exchange_rate`, `status`, `code`, `created_at`, `updated_at`) VALUES
(1, 'U.S. Dollar', '$', 1.00000, 1, 'USD', '2018-10-09 11:35:08', '2018-10-17 05:50:52'),
(2, 'Australian Dollar', '$', 1.28000, 1, 'AUD', '2018-10-09 11:35:08', '2019-02-04 05:51:55'),
(5, 'Brazilian Real', 'R$', 3.25000, 1, 'BRL', '2018-10-09 11:35:08', '2018-10-17 05:51:00'),
(6, 'Canadian Dollar', '$', 1.27000, 1, 'CAD', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(7, 'Czech Koruna', 'Kč', 20.65000, 1, 'CZK', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(8, 'Danish Krone', 'kr', 6.05000, 1, 'DKK', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(9, 'Euro', '€', 0.85000, 1, 'EUR', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(10, 'Hong Kong Dollar', '$', 7.83000, 1, 'HKD', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(11, 'Hungarian Forint', 'Ft', 255.24000, 1, 'HUF', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(12, 'Israeli New Sheqel', '₪', 3.48000, 1, 'ILS', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(13, 'Japanese Yen', '¥', 107.12000, 1, 'JPY', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(14, 'Malaysian Ringgit', 'RM', 3.91000, 1, 'MYR', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(15, 'Mexican Peso', '$', 18.72000, 1, 'MXN', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(16, 'Norwegian Krone', 'kr', 7.83000, 1, 'NOK', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(17, 'New Zealand Dollar', '$', 1.38000, 1, 'NZD', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(18, 'Philippine Peso', '₱', 52.26000, 1, 'PHP', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(19, 'Polish Zloty', 'zł', 3.39000, 1, 'PLN', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(20, 'Pound Sterling', '£', 0.72000, 1, 'GBP', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(21, 'Russian Ruble', 'руб', 55.93000, 1, 'RUB', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(22, 'Singapore Dollar', '$', 1.32000, 1, 'SGD', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(23, 'Swedish Krona', 'kr', 8.19000, 1, 'SEK', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(24, 'Swiss Franc', 'CHF', 0.94000, 1, 'CHF', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(26, 'Thai Baht', '฿', 31.39000, 1, 'THB', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(27, 'Taka', '৳', 84.00000, 1, 'BDT', '2018-10-09 11:35:08', '2018-12-02 05:16:13'),
(28, 'Indian Rupee', 'Rs', 68.45000, 1, 'Rupee', '2019-07-07 10:33:46', '2019-07-07 10:33:46');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `user_id`, `created_at`, `updated_at`) VALUES
(4, 8, '2019-08-01 10:35:09', '2019-08-01 10:35:09');

-- --------------------------------------------------------

--
-- Table structure for table `customer_packages`
--

CREATE TABLE `customer_packages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double(28,2) DEFAULT NULL,
  `product_upload` int(11) DEFAULT NULL,
  `logo` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_products`
--

CREATE TABLE `customer_products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published` int(1) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 0,
  `added_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `subsubcategory_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `photos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail_img` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conditon` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_provider` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_link` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_price` double(28,2) DEFAULT 0.00,
  `meta_title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_img` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdf` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flash_deals`
--

CREATE TABLE `flash_deals` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` int(20) DEFAULT NULL,
  `end_date` int(20) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `featured` int(1) NOT NULL DEFAULT 0,
  `background_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flash_deal_products`
--

CREATE TABLE `flash_deal_products` (
  `id` int(11) NOT NULL,
  `flash_deal_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `discount` double(8,2) DEFAULT 0.00,
  `discount_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `franchises`
--

CREATE TABLE `franchises` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT 0,
  `verification_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referal_code` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `franchise_state_id` int(11) DEFAULT NULL,
  `franchise_image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship_front` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship_back` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `franchises`
--

INSERT INTO `franchises` (`id`, `user_id`, `verification_status`, `verification_info`, `referal_code`, `franchise_state_id`, `franchise_image`, `citizenship_front`, `citizenship_back`, `created_at`, `updated_at`) VALUES
(1, 29, 0, NULL, 'LSXV-UARI', 1, NULL, NULL, NULL, '2020-07-31 11:18:02', '2020-07-31 11:18:02');

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(11) NOT NULL,
  `frontend_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_login_background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_login_sidebar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_plus` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `frontend_color`, `logo`, `admin_logo`, `admin_login_background`, `admin_login_sidebar`, `favicon`, `site_name`, `address`, `description`, `phone`, `email`, `facebook`, `instagram`, `twitter`, `youtube`, `google_plus`, `created_at`, `updated_at`) VALUES
(1, 'default', 'uploads/logo/dzf3WQwboc27vHyVvR1KATgEinVfcqHzD3cpUhbZ.png', 'uploads/admin_logo/drmFoQfzJ55AhdaMHAIfzDlgZeToMdHm3jtm5g8v.png', 'uploads/admin_login_background/ITIq0hCCQpgvQR8JdiyDqBU7ffZfvHBhQYVswLK1.jpeg', 'uploads/admin_login_sidebar/TENQOSYACgEFPDD4scx9F0HHClXGTfbMCxQXroA5.png', 'uploads/favicon/HLs2yHZFayM4wncLmjW6AYKp3M3dmj3dgn8ql7Qd.png', 'Nep Shopy', 'Gwarkho, lalitpur', 'Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.', '9851275055', 'nepshopy@gmail.com', 'https://www.facebook.com/nepshopynp', 'https://www.instagram.com/nepshopynp', 'https://www.twitter.com/nepshopy1', 'https://www.youtube.com/nepshopynp', 'https://www.googleplus.com/nepshopynp', '2020-08-17 15:38:23', '2020-08-18 03:23:23');

-- --------------------------------------------------------

--
-- Table structure for table `home_categories`
--

CREATE TABLE `home_categories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subsubcategories` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_categories`
--

INSERT INTO `home_categories` (`id`, `category_id`, `subsubcategories`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '[\"1\"]', 1, '2019-03-12 06:38:23', '2019-03-12 06:38:23'),
(2, 2, '[\"10\"]', 1, '2019-03-12 06:44:54', '2019-03-12 06:44:54'),
(3, 4, 'null', 1, '2020-07-28 03:51:15', '2020-07-28 03:51:15'),
(4, 6, 'null', 1, '2020-07-30 07:00:45', '2020-07-30 07:00:45');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rtl` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `code`, `rtl`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', 0, '2019-01-20 12:13:20', '2019-01-20 12:13:20'),
(3, 'Bangla', 'bd', 0, '2019-02-17 06:35:37', '2019-02-18 06:49:51'),
(4, 'Arabic', 'sa', 1, '2019-04-28 18:34:12', '2019-04-28 18:34:12');

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `conversation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf32_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `conversation_id`, `user_id`, `message`, `created_at`, `updated_at`) VALUES
(8, 2, 20, 'http://seller.nepshopy.com/product/Brown-Lace-Up-Glossy-Formal-Shoes-For-Men-G-917-Kd9j0\nHello test', '2020-07-17 08:07:42', '2020-07-17 08:07:42'),
(9, 3, 20, 'http://seller.nepshopy.com/product/Brown-Lace-Up-Glossy-Formal-Shoes-For-Men-G-917-Kd9j0\nHello test', '2020-07-17 08:07:43', '2020-07-17 08:07:43'),
(10, 2, 21, 'hii', '2020-07-17 08:08:19', '2020-07-17 08:08:19'),
(11, 2, 20, 'Is my product ready', '2020-07-17 08:08:58', '2020-07-17 08:08:58'),
(12, 2, 21, 'it is indeed ready.', '2020-07-17 08:09:38', '2020-07-17 08:09:38'),
(13, 4, 12, 'https://nepshopy.com/product/hello-product-OxVLG', '2020-08-05 01:28:38', '2020-08-05 01:28:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('125ce8289850f80d9fea100325bf892fbd0deba1f87dbfc2ab81fb43d57377ec24ed65f7dc560e46', 1, 1, 'Personal Access Token', '[]', 0, '2019-07-30 04:51:13', '2019-07-30 04:51:13', '2020-07-30 10:51:13'),
('293d2bb534220c070c4e90d25b5509965d23d3ddbc05b1e29fb4899ae09420ff112dbccab1c6f504', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 06:00:04', '2019-08-04 06:00:04', '2020-08-04 12:00:04'),
('5363e91c7892acdd6417aa9c7d4987d83568e229befbd75be64282dbe8a88147c6c705e06c1fb2bf', 1, 1, 'Personal Access Token', '[]', 0, '2019-07-13 06:44:28', '2019-07-13 06:44:28', '2020-07-13 12:44:28'),
('681b4a4099fac5e12517307b4027b54df94cbaf0cbf6b4bf496534c94f0ccd8a79dd6af9742d076b', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 07:23:06', '2019-08-04 07:23:06', '2020-08-04 13:23:06'),
('6d229e3559e568df086c706a1056f760abc1370abe74033c773490581a042442154afa1260c4b6f0', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 07:32:12', '2019-08-04 07:32:12', '2020-08-04 13:32:12'),
('6efc0f1fc3843027ea1ea7cd35acf9c74282f0271c31d45a164e7b27025a315d31022efe7bb94aaa', 1, 1, 'Personal Access Token', '[]', 0, '2019-08-08 02:35:26', '2019-08-08 02:35:26', '2020-08-08 08:35:26'),
('7745b763da15a06eaded371330072361b0524c41651cf48bf76fc1b521a475ece78703646e06d3b0', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 07:29:44', '2019-08-04 07:29:44', '2020-08-04 13:29:44'),
('815b625e239934be293cd34479b0f766bbc1da7cc10d464a2944ddce3a0142e943ae48be018ccbd0', 1, 1, 'Personal Access Token', '[]', 1, '2019-07-22 02:07:47', '2019-07-22 02:07:47', '2020-07-22 08:07:47'),
('8921a4c96a6d674ac002e216f98855c69de2568003f9b4136f6e66f4cb9545442fb3e37e91a27cad', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 06:05:05', '2019-08-04 06:05:05', '2020-08-04 12:05:05'),
('8d8b85720304e2f161a66564cec0ecd50d70e611cc0efbf04e409330086e6009f72a39ce2191f33a', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 06:44:35', '2019-08-04 06:44:35', '2020-08-04 12:44:35'),
('bcaaebdead4c0ef15f3ea6d196fd80749d309e6db8603b235e818cb626a5cea034ff2a55b66e3e1a', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 07:14:32', '2019-08-04 07:14:32', '2020-08-04 13:14:32'),
('c25417a5c728073ca8ba57058ded43d496a9d2619b434d2a004dd490a64478c08bc3e06ffc1be65d', 1, 1, 'Personal Access Token', '[]', 1, '2019-07-30 01:45:31', '2019-07-30 01:45:31', '2020-07-30 07:45:31'),
('c7423d85b2b5bdc5027cb283be57fa22f5943cae43f60b0ed27e6dd198e46f25e3501b3081ed0777', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-05 05:02:59', '2019-08-05 05:02:59', '2020-08-05 11:02:59'),
('e76f19dbd5c2c4060719fb1006ac56116fd86f7838b4bf74e2c0a0ac9696e724df1e517dbdb357f4', 1, 1, 'Personal Access Token', '[]', 1, '2019-07-15 02:53:40', '2019-07-15 02:53:40', '2020-07-15 08:53:40'),
('ed7c269dd6f9a97750a982f62e0de54749be6950e323cdfef892a1ec93f8ddbacf9fe26e6a42180e', 1, 1, 'Personal Access Token', '[]', 1, '2019-07-13 06:36:45', '2019-07-13 06:36:45', '2020-07-13 12:36:45'),
('f6d1475bc17a27e389000d3df4da5c5004ce7610158b0dd414226700c0f6db48914637b4c76e1948', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 07:22:01', '2019-08-04 07:22:01', '2020-08-04 13:22:01'),
('f85e4e444fc954430170c41779a4238f84cd6fed905f682795cd4d7b6a291ec5204a10ac0480eb30', 1, 1, 'Personal Access Token', '[]', 1, '2019-07-30 06:38:49', '2019-07-30 06:38:49', '2020-07-30 12:38:49'),
('f8bf983a42c543b99128296e4bc7c2d17a52b5b9ef69670c629b93a653c6a4af27be452e0c331f79', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 07:28:55', '2019-08-04 07:28:55', '2020-08-04 13:28:55');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'eR2y7WUuem28ugHKppFpmss7jPyOHZsMkQwBo1Jj', 'http://localhost', 1, 0, 0, '2019-07-13 06:17:34', '2019-07-13 06:17:34'),
(2, NULL, 'Laravel Password Grant Client', 'WLW2Ol0GozbaXEnx1NtXoweYPuKEbjWdviaUgw77', 'http://localhost', 0, 1, 0, '2019-07-13 06:17:34', '2019-07-13 06:17:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-07-13 06:17:34', '2019-07-13 06:17:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `shipping_address` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'unpaid',
  `payment_details` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `grand_total` double(8,2) DEFAULT NULL,
  `coupon_discount` double(8,2) NOT NULL DEFAULT 0.00,
  `code` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` int(20) NOT NULL,
  `viewed` int(1) NOT NULL DEFAULT 0,
  `delivery_viewed` int(1) NOT NULL DEFAULT 1,
  `payment_status_viewed` int(1) DEFAULT 1,
  `commission_calculated` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `variation` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `tax` double(8,2) NOT NULL DEFAULT 0.00,
  `shipping_cost` double(8,2) NOT NULL DEFAULT 0.00,
  `quantity` int(11) DEFAULT NULL,
  `payment_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unpaid',
  `delivery_status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'pending',
  `shipping_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pickup_point_id` int(11) DEFAULT NULL,
  `product_referral_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `content`, `meta_title`, `meta_description`, `keywords`, `meta_image`, `created_at`, `updated_at`) VALUES
(1, 'NPR', 'orange', 'Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.', 'sonyinc', 'Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.', 'Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.', NULL, '2020-07-13 03:44:55', '2020-07-13 03:44:55');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT 0.00,
  `payment_details` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `txn_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pickup_points`
--

CREATE TABLE `pickup_points` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(15) NOT NULL,
  `pick_up_status` int(1) DEFAULT NULL,
  `cash_on_pickup_status` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pickup_points`
--

INSERT INTO `pickup_points` (`id`, `staff_id`, `name`, `address`, `phone`, `pick_up_status`, `cash_on_pickup_status`, `created_at`, `updated_at`) VALUES
(6, 1, 'Nep Shopy', 'Imdol, Gwarko, Lalitpur.', '9851275055', 1, NULL, '2020-07-17 07:47:13', '2020-07-17 07:47:13');

-- --------------------------------------------------------

--
-- Table structure for table `policies`
--

CREATE TABLE `policies` (
  `id` int(11) NOT NULL,
  `name` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `policies`
--

INSERT INTO `policies` (`id`, `name`, `content`, `created_at`, `updated_at`) VALUES
(1, 'support_policy', NULL, '2019-10-29 12:54:45', '2019-01-22 05:13:15'),
(2, 'return_policy', NULL, '2019-10-29 12:54:47', '2019-01-24 05:40:11'),
(4, 'seller_policy', NULL, '2019-10-29 12:54:49', '2019-02-04 17:50:15'),
(5, 'terms', NULL, '2019-10-29 12:54:51', '2019-10-28 18:00:00'),
(6, 'privacy_policy', NULL, '2019-10-29 12:54:54', '2019-10-28 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin',
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `subsubcategory_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `photos` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attribute_image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flash_deal_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_provider` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_link` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tags` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_price` double(8,2) NOT NULL,
  `purchase_price` double(8,2) NOT NULL,
  `variant_product` int(1) NOT NULL DEFAULT 0,
  `attributes` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `choice_options` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `colors` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `variations` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `todays_deal` int(11) NOT NULL DEFAULT 0,
  `published` int(11) NOT NULL DEFAULT 1,
  `featured` int(11) NOT NULL DEFAULT 0,
  `current_stock` int(10) NOT NULL DEFAULT 0,
  `unit` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `discount_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax` double(8,2) DEFAULT NULL,
  `tax_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_type` varchar(20) CHARACTER SET latin1 DEFAULT 'flat_rate',
  `shipping_cost` double(8,2) DEFAULT 0.00,
  `num_of_sale` int(11) NOT NULL DEFAULT 0,
  `meta_title` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `refundable` int(1) NOT NULL DEFAULT 0,
  `rating` double(8,2) NOT NULL DEFAULT 0.00,
  `barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `digital` int(1) NOT NULL DEFAULT 0,
  `file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `added_by`, `user_id`, `category_id`, `subcategory_id`, `subsubcategory_id`, `brand_id`, `photos`, `attribute_image`, `thumbnail_img`, `featured_img`, `flash_deal_img`, `video_provider`, `video_link`, `tags`, `description`, `unit_price`, `purchase_price`, `variant_product`, `attributes`, `choice_options`, `colors`, `variations`, `todays_deal`, `published`, `featured`, `current_stock`, `unit`, `discount`, `discount_type`, `tax`, `tax_type`, `shipping_type`, `shipping_cost`, `num_of_sale`, `meta_title`, `meta_description`, `meta_img`, `pdf`, `slug`, `refundable`, `rating`, `barcode`, `digital`, `file_name`, `file_path`, `created_at`, `updated_at`) VALUES
(79, 'Rayon Anarkali Kurti with Dupatta', 'seller', 33, 1, 2, 30, NULL, '[\"uploads\\/products\\/photos\\/LVachgayABbbgXZAE0sd8LweynSZbidpuDrJzBmr.jpeg\",\"uploads\\/products\\/photos\\/zY8muAzHDLaEuhJNGjEu8L90tFJh45mraNmOiwws.jpeg\"]', '[{\"color_image\":\"uploads\\/products\\/photos\\/oWWoidu11w53RpbOQcUbnwQGVjYa18Kyuu4VR44g.jpeg\",\"color_name\":\"#6495ED\"}]', 'uploads/products/thumbnail/Yc1rBwFOU6PV8I4OGNZhA45xY3lrfrBzHoGLLpfr.jpeg', NULL, NULL, 'youtube', NULL, 'anarkali kurti, beautiful kurta, branded kurti, rayon kurta, woman kurta, woman kurti', '<p><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--><strong>Product Details</strong></p><ul><li>Type: Kurti Fabric</li><li>Ideal for: Women</li><li>Color:Blue</li><li>Fit: Regular</li><li>Occasion: Casual,&nbsp;Festive</li><li>Sleeve: ¾ Sleeve</li><li>Neck Type: Round Neck</li><li>Work Type: Embroidery</li><li>Fabric: Rayon anarkali kurta with floral embroidery</li><li>Fabric Care: Gentle hand or machine wash, use mild detergent and dry in shade</li></ul>', 3500.00, 0.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"XL\"]}]', '[\"#6495ED\"]', '[]', 0, 1, 0, 0, '1', 0.00, 'amount', 0.00, 'amount', NULL, 0.00, 0, NULL, NULL, NULL, NULL, 'Rayon-Anarkali-Kurti-with-Dupatta-SXU3E', 1, 0.00, NULL, 0, NULL, NULL, '2020-08-18 22:04:40', '2020-08-18 22:37:33'),
(80, 'Rayon Anarkali Kurti with Dupatta', 'seller', 33, 1, 2, 30, NULL, '[\"uploads\\/products\\/photos\\/MrUFGtBvrBV5NChzjc0KZhxLkUH6Tn9kvSFiTrrJ.jpeg\",\"uploads\\/products\\/photos\\/wOUXV1sk0YTqW7KVZtqoeZna757kZ4QtICrBUOmK.jpeg\"]', '[]', 'uploads/products/thumbnail/9eoWubK5lQaD5C7i2aSEA8FMWgPA6Il77ZIVQuKr.jpeg', NULL, NULL, 'youtube', NULL, 'anarkali kurti, beautiful kurta, branded kurti, rayon kurta, woman kurta, woman kurti', '<p><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--><strong>Product Details</strong></p><ul><li>Type: Kurti Fabric</li><li>Ideal for: Women</li><li>Color: Pink</li><li>Fit: Regular</li><li>Occasion: Casual,&nbsp;Festive</li><li>Sleeve: ¾ Sleeve</li><li>Neck Type: Round Neck</li><li>Work Type: Embroidery</li><li>Fabric: Rayon anarkali kurta with floral embroidery</li><li>Fabric Care: Gentle hand or machine wash, use mild detergent and dry in shade</li></ul>', 3500.00, 0.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"L\"]}]', '[\"#FFC0CB\"]', '[]', 0, 1, 0, 0, '1', 0.00, 'amount', 0.00, 'amount', NULL, 0.00, 0, NULL, NULL, 'uploads/products/meta/S9w7FT1YMft7sqmHpvT0ktJ2RuFjVcE2v5uIMmit.jpeg', NULL, 'Rayon-Anarkali-Kurti-with-Dupatta-bxPqr', 1, 0.00, NULL, 0, NULL, NULL, '2020-08-18 22:08:10', '2020-08-18 22:36:13'),
(81, 'Rayon Anarkali Kurti with Dupatta', 'seller', 33, 1, 2, 30, NULL, '[\"uploads\\/products\\/photos\\/wHBWrOD8xAaw9wQv8sR6XeRCbdDrSzjGAOj0VKvE.jpeg\"]', '[{\"color_image\":\"uploads\\/products\\/photos\\/AmrmXROlpzStpQxDiwXMemsyPsvsxQTn5miUQlOM.jpeg\",\"color_name\":\"#8FBC8F\"}]', 'uploads/products/thumbnail/pcBAMGTO1d7ef5HliRhDlgBoVxso6yTfii26IFcx.jpeg', NULL, NULL, 'youtube', NULL, 'anarkali kurti, beautiful kurta, branded kurti, rayon kurta, woman kurta, woman kurti', '<p><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--><strong>Product Details</strong></p><ul><li>Type: Kurti Fabric</li><li>Ideal for: Women</li><li>Color: Green</li><li>Fit: Regular</li><li>Occasion: Casual,&nbsp;Festive</li><li>Sleeve: ¾ Sleeve</li><li>Neck Type: Round Neck</li><li>Work Type: Embroidery</li><li>Fabric: Rayon anarkali kurta with floral embroidery</li><li>Fabric Care: Gentle hand or machine wash, use mild detergent and dry in shade</li></ul>', 3500.00, 0.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"XL\"]}]', '[\"#8FBC8F\"]', '[]', 0, 1, 0, 0, '1', 0.00, 'amount', 0.00, 'amount', NULL, 0.00, 0, NULL, NULL, NULL, NULL, 'Rayon-Anarkali-Kurti-with-Dupatta-9jxPH', 1, 0.00, NULL, 0, NULL, NULL, '2020-08-18 22:15:31', '2020-08-18 22:33:47'),
(82, 'Rayon Anarkali Kurti with Dupatta', 'seller', 33, 1, 2, 30, NULL, '[\"uploads\\/products\\/photos\\/eeCaknNodKdDcLosMmYGi2BmIkB2zffKsIn4l78r.jpeg\"]', '[{\"color_image\":\"uploads\\/products\\/photos\\/Pf19ptYOh9dLpBBVI0mHLRApbphRwIctHE0SdjEO.jpeg\",\"color_name\":\"#CD5C5C\"}]', 'uploads/products/thumbnail/oLspTvkt7ZnXQIC4oHV7w7lCQZQf4j5D1yiyrm5o.jpeg', NULL, NULL, 'youtube', NULL, 'anarkali kurti, beautiful kurta, branded kurti, rayon kurta, woman kurta, woman kurti', '<p><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--><strong>Product Details</strong></p><ul><li>&nbsp;Type: Kurti Fabric</li><li>Ideal for: Women</li><li>Color: Red</li><li>Fabric Type: Cotton</li><li>Length: Medium</li><li>Neck Type: Chinese Collar</li><li>Size Set / Color Set: Size Set</li><li>Sleeve: 3/4 Sleeve</li><li>Style: Straight</li><li>Occasion: Casual</li><li>Work Type: Gold Printed</li><li>Fabric Care: Gentle hand or machine wash, use mild detergent and dry in shade&nbsp; &nbsp; &nbsp;</li></ul>', 3500.00, 0.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"XL\"]}]', '[\"#CD5C5C\"]', '[]', 0, 1, 0, 0, '1', 0.00, 'amount', 0.00, 'amount', NULL, 0.00, 0, NULL, NULL, NULL, NULL, 'Rayon-Anarkali-Kurti-with-Dupatta-Omw0C', 1, 0.00, NULL, 0, NULL, NULL, '2020-08-18 22:23:45', '2020-08-18 22:38:02'),
(83, 'Samsung Galaxy M31 [6GB RAM // 128GB ROM]', 'seller', 34, 3, 11, 62, 8, '[\"uploads\\/products\\/photos\\/SLABZWw8w9xIURnb9YJQO2ug2WvrElw9D2KbY4Ea.jpeg\"]', '[{\"color_image\":\"uploads\\/products\\/photos\\/sAVqwqRUp1bSn51pHVQm2S0R6Io88OL3mYObmAMe.jpeg\",\"color_name\":\"#000000\"},{\"color_image\":\"uploads\\/products\\/photos\\/TAbXRRkcPTeXmgDD9e7DBtswHFF4t7DhgXn97ISa.jpeg\",\"color_name\":\"#8A2BE2\"}]', 'uploads/products/thumbnail/zsV2dH2hYgbXrEP1sVnFySxlEaouukirIJT4WtHM.jpeg', NULL, NULL, 'youtube', NULL, 'android mobile,smartphone', '<p class=\"pdp-mod-section-title outer-title\"><strong>Product details of Samsung Galaxy M31 [6GB RAM // 128GB ROM]</strong></p><div class=\"pdp-product-detail\" data-spm=\"product_detail\"><div class=\"pdp-product-desc \" data-spm-anchor-id=\"a2a0e.pdp.product_detail.i2.28b691ccbbsvD4\"><div class=\"html-content pdp-product-highlights\"><ul class=\"\"><li class=\"\">48MP Tripple rear camera6000mAH Monster Battery6.4\" Sumer Amoled Display</li></ul></div><div class=\"pdp-mod-specification\"><p class=\"pdp-mod-section-title \" style=\"text-align: left;\"><strong>Specifications of Samsung Galaxy M31 [6GB RAM // 128GB ROM]</strong></p><div class=\"pdp-general-features\"><ul class=\"specification-keys\"><li class=\"key-li\"><span class=\"key-title\"> Brand  </span><div class=\"html-content key-value\">Samsung</div></li><li class=\"key-li\"><span class=\"key-title\"> SKU  </span><div class=\"html-content key-value\">103978475_NP-1024746557</div></li><li class=\"key-li\"><span class=\"key-title\"> Protection  </span><div class=\"html-content key-value\">Corning Gorilla Glass 3</div></li><li class=\"key-li\"><span class=\"key-title\"> Body Type  </span><div class=\"html-content key-value\">Plastic</div></li><li class=\"key-li\"><span class=\"key-title\"> Phone Type  </span><div class=\"html-content key-value\">Smartphone</div></li><li class=\"key-li\"><span class=\"key-title\"> Year  </span><div class=\"html-content key-value\">2020</div></li><li class=\"key-li\"><span class=\"key-title\"> Number Of Cameras  </span><div class=\"html-content key-value\">4</div></li><li class=\"key-li\"><span class=\"key-title\"> Wireless Charging  </span><div class=\"html-content key-value\">No</div></li><li class=\"key-li\"><span class=\"key-title\"> Fast Charging  </span><div class=\"html-content key-value\">Yes</div></li><li class=\"key-li\"><span class=\"key-title\"> Processor Type  </span><div class=\"html-content key-value\">Other</div></li><li class=\"key-li\"><span class=\"key-title\"> Screen Size (inches)  </span><div class=\"html-content key-value\">6 Inches &amp; Above</div></li></ul></div><div class=\"box-content\" data-spm-anchor-id=\"a2a0e.pdp.product_detail.i3.28b691ccbbsvD4\"><span class=\"key-title\">What’s in the box</span><div class=\"html-content box-content-html\">1x Phone, 1x Charger, 1x Cable</div></div></div></div></div>', 29999.01, 27000.00, 1, '[\"4\",\"7\"]', '[{\"attribute_id\":\"4\",\"values\":[\"6GB\"]},{\"attribute_id\":\"7\",\"values\":[\"5\'\'\"]}]', '[\"#000000\",\"#8A2BE2\"]', '[]', 0, 1, 0, 0, '1', 0.00, 'percent', 0.00, 'percent', NULL, 0.00, 0, NULL, NULL, NULL, NULL, 'Samsung-Galaxy-M31-6GB-RAM--128GB-ROM-Zj7T0', 1, 0.00, NULL, 0, NULL, NULL, '2020-08-18 22:45:28', '2020-08-18 22:45:28'),
(84, 'Cotton Kurti with Pants', 'seller', 33, 1, 2, 30, NULL, '[\"uploads\\/products\\/photos\\/RHpXyYMM8CKsGyPzlzSz9Gretl7IdhLvhbxNM5dB.jpeg\"]', '[{\"color_image\":\"uploads\\/products\\/photos\\/1EIua2UPxY9z8QmXJAQ2EjZ0Gf3p0vwV7H3yctbu.jpeg\",\"color_name\":\"#0000FF\"}]', 'uploads/products/thumbnail/LrZhHC6wBDQQEixJBPzBTw9RPdr98Yxh5kkUePxR.jpeg', NULL, NULL, 'youtube', NULL, 'cotton kurta, cotton kurti, golden patterns kurta and pants, kurta with pants, ladies kurta with pants, royal blue kurta with pants', '<p><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}-->Product Details</p><p>Type: Kurti Fabric</p><p>Ideal for: Women</p><p>Occasion: Casual, Party and Festive.</p><p>Fabric Care: Gentle hand or machine wash, use mild detergent and dry in shade</p><p>Color: blue</p><p>Fabric: cotton silk</p><p>Ankle length anarkli south cotton kurti with half sleeves</p><p>Heavy embroidery on upper body</p><p>It\'s suitable for party &amp; festive wear</p><p>Gentle wash separately in cold water</p><p>Give you awesome, gorgeous &amp; attractive look when you attire this kurti</p><p>Light green south self-weaved slub chanderi gown with heavy embroidery on upper body and sleeves, cotton lining,pleated flair and heavy front buttons. &nbsp;</p>', 2740.00, 0.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"XXL\"]}]', '[\"#0000FF\"]', '[]', 0, 1, 0, 0, '1', 0.00, 'percent', 0.00, 'percent', NULL, 0.00, 0, NULL, NULL, NULL, NULL, 'Cotton-Kurti-with-Pants-MWCvI', 1, 0.00, NULL, 0, NULL, NULL, '2020-08-18 22:47:18', '2020-08-18 22:47:18'),
(85, 'Women\'s Mustard and Light Green Cotton Silk Kurti With Embroidery', 'seller', 33, 1, 2, 30, NULL, '[\"uploads\\/products\\/photos\\/Ubcja8EtwSMs90AwPmAXmls2wlYnHPLNiU02EegG.jpeg\"]', '[{\"color_image\":\"uploads\\/products\\/photos\\/QpvLvXIiUYDbmK0ckIvzEFP0EABwn8kjC1xf90EM.jpeg\",\"color_name\":\"#8FBC8F\"}]', 'uploads/products/thumbnail/F3lDiGzViWbTQYlzVglSetnjTtrTAen6m0ed8F10.jpeg', NULL, NULL, 'youtube', NULL, 'branded kurti, kurta and kurti, light green kurti, stylish kurti, woman embroidered kurti', '<p><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--><strong>Product Details</strong></p><ul><li>Type : A-line</li><li>Fabric : Georgette</li><li>Ideal For : Women</li><li>Neck : Round Neck</li><li>Pattern : Embroidered</li><li>Legging Available : No</li><li>Beautiful Kurti for Beautiful girls</li></ul>', 4000.00, 0.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"XL\"]}]', '[\"#8FBC8F\"]', '[]', 0, 1, 0, 0, '1', 0.00, 'percent', 0.00, 'percent', NULL, 0.00, 0, NULL, NULL, NULL, NULL, 'Womens-Mustard-and-Light-Green-Cotton-Silk-Kurti-With-Embroidery-GdPxd', 1, 0.00, NULL, 0, NULL, NULL, '2020-08-18 22:59:05', '2020-08-18 22:59:05'),
(86, 'Casual Embroidered Women Kurti', 'seller', 33, 1, 2, 30, NULL, '[\"uploads\\/products\\/photos\\/cVhPt0cA7DHDceG3KvPn9ESPrxQzBhWHUKyrf8dq.jpeg\"]', '[{\"color_image\":\"uploads\\/products\\/photos\\/2ihIIGsFgXCiNMFMiDKHfgfk36IxXebJCMK5n9LN.jpeg\",\"color_name\":\"#90EE90\"}]', 'uploads/products/thumbnail/HdDfOhZQcgq31hjLIyTsio0246cLQSFIWGmebUpc.jpeg', NULL, NULL, 'youtube', NULL, 'embroidered woman kurti, georgette woman kurti, kurti, stylish kurti, woman kurti', '<p><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--><strong>Product Details</strong></p><ul><li>Type : A-line</li><li>Fabric : Georgette</li><li>Ideal For : Women</li><li>Neck : Round Neck</li><li>Pattern : Embroidered</li><li>Legging Available : No</li><li>Beautiful Kurti for Beautiful girls</li></ul>', 2000.00, 0.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"XL\"]}]', '[\"#90EE90\"]', '[]', 0, 1, 0, 0, '1', 0.00, 'percent', 0.00, 'percent', NULL, 0.00, 0, NULL, NULL, NULL, NULL, 'Casual-Embroidered-Women-Kurti-fHiuX', 1, 0.00, NULL, 0, NULL, NULL, '2020-08-18 23:02:52', '2020-08-18 23:02:52'),
(87, 'Casual Embroidered Women Kurti (Navy Blue)', 'seller', 33, 1, 2, 30, NULL, '[\"uploads\\/products\\/photos\\/ztWG4KENUK6MXYATCS9aXtdAEEBceDxZd1blsgsA.jpeg\"]', '[{\"color_image\":\"uploads\\/products\\/photos\\/efYMZFwzjRQv8DDhab2gIwIaN3cpYWB5IxosZ2fo.jpeg\",\"color_name\":\"#00008B\"}]', 'uploads/products/thumbnail/TcLfgJr8NdmnA4B5yEDxG3msbrkLDHWS4t3hhex1.jpeg', NULL, NULL, 'youtube', NULL, 'embroidered woman kurti, georgette woman kurti, kurti, navy blue kurti, stylish kurti, woman kurti', '<p><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--><strong>Product Details</strong></p><ul><li>Type : A-line</li><li>Fabric : Georgette</li><li>Ideal For : Women</li><li>Neck : Round Neck</li><li>Pattern : Embroidered</li><li>Legging Available : No</li><li>Beautiful Kurti for Beautiful girls</li></ul>', 2000.00, 0.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"XL\"]}]', '[\"#00008B\"]', '[]', 0, 1, 0, 0, '1', 0.00, 'percent', 0.00, 'percent', NULL, 0.00, 0, NULL, NULL, NULL, NULL, 'Casual-Embroidered-Women-Kurti-Navy-Blue-qEpZi', 1, 0.00, NULL, 0, NULL, NULL, '2020-08-18 23:05:49', '2020-08-18 23:05:49'),
(88, 'Casual Embroidered Women Kurti (Black)', 'seller', 33, 1, 2, 30, NULL, '[\"uploads\\/products\\/photos\\/DkDS20bAf5LlkybyKqUv2ZntGgZLFHShGcrGZL0F.jpeg\"]', '[{\"color_image\":\"uploads\\/products\\/photos\\/sulAN9pXBMObNQ4GOuFm29taGWH6oi2lYk7B5zkx.jpeg\",\"color_name\":\"#006400\"}]', 'uploads/products/thumbnail/d7cqqau7poWZjA0TfFu8X4J4qPGHoVQOWV5bjqg5.jpeg', NULL, NULL, 'youtube', NULL, 'black kurti, embroidered woman kurti, georgette woman kurti, kurti, stylish kurti, woman kurti', '<p><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--><strong>Product Details</strong></p><ul><li>Type : A-line</li><li>Fabric : Georgette</li><li>Ideal For : Women</li><li>Neck : Round Neck</li><li>Pattern : Embroidered</li><li>Legging Available : No</li><li>Beautiful Kurti for Beautiful girls</li></ul>', 2000.00, 0.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"XL\"]}]', '[\"#006400\"]', '[]', 0, 1, 0, 0, '1', 0.00, 'percent', 0.00, 'percent', NULL, 0.00, 0, NULL, NULL, NULL, NULL, 'Casual-Embroidered-Women-Kurti-Black-Q2ToC', 1, 0.00, NULL, 0, NULL, NULL, '2020-08-18 23:09:49', '2020-08-18 23:09:49'),
(89, 'Casual Embroidered Women Kurti (White)', 'seller', 33, 1, 2, 30, NULL, '[\"uploads\\/products\\/photos\\/rkKidp3Uy8CMRWATnK2bxwpXA5xWAAKIMZ38MwwW.jpeg\"]', '[{\"color_image\":\"uploads\\/products\\/photos\\/PUCOZY0JoK3FSdk05WGHuKHaUMnjaUF0V9dluZXv.jpeg\",\"color_name\":\"#FFFFFF\"}]', 'uploads/products/thumbnail/u7hY9P3XZmXSmLag0i2ZAXjMliLzjj1hFJWDsnX7.jpeg', NULL, NULL, 'youtube', NULL, 'embroidered woman kurti, georgette woman kurti, kurti, stylish kurti, white kurti, woman kurti', '<p><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--><strong>Product Details</strong></p><ul><li>Type : A-line</li><li>Fabric : Georgette</li><li>Ideal For : Women</li><li>Neck : Round Neck</li><li>Pattern : Embroidered</li><li>Legging Available : No</li><li>Beautiful Kurti for Beautiful girls</li></ul>', 2000.00, 0.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"XL\"]}]', '[\"#FFFFFF\"]', '[]', 0, 1, 0, 0, '1', 0.00, 'percent', 0.00, 'percent', NULL, 0.00, 0, NULL, NULL, NULL, NULL, 'Casual-Embroidered-Women-Kurti-White-zY3Yb', 1, 0.00, NULL, 0, NULL, NULL, '2020-08-18 23:20:00', '2020-08-18 23:20:00'),
(90, 'Casual Embroidered Women Kurti (Dark Green)', 'seller', 33, 1, 2, 30, NULL, '[\"uploads\\/products\\/photos\\/wAo08v04glziPlcVAqUK556Zb6UNPVf2XxiAyXED.jpeg\"]', '[{\"color_image\":\"uploads\\/products\\/photos\\/5hzslaScldKqyscle7h9zG4JwkkwZyGJCHuFjmbg.jpeg\",\"color_name\":\"#006400\"}]', 'uploads/products/thumbnail/S8bnf3CNgFgc2YB46UtaMcyy4rjl931GyuLzoFqk.jpeg', NULL, NULL, 'youtube', NULL, 'dark green kurti, embroidered woman kurti, georgette woman kurti, kurti, stylish kurti, woman kurti', '<p><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--><strong>Product Details</strong></p><ul><li>Type : A-line</li><li>Fabric : Georgette</li><li>Ideal For : Women</li><li>Neck : Round Neck</li><li>Pattern : Embroidered</li><li>Legging Available : No</li><li>Beautiful Kurti for Beautiful girls</li></ul>', 2000.00, 0.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"XL\"]}]', '[\"#006400\"]', '[]', 0, 1, 0, 0, '1', 0.00, 'percent', 0.00, 'percent', NULL, 0.00, 0, NULL, NULL, NULL, NULL, 'Casual-Embroidered-Women-Kurti-Dark-Green-ctrJu', 1, 0.00, NULL, 0, NULL, NULL, '2020-08-18 23:23:19', '2020-08-18 23:23:19');

-- --------------------------------------------------------

--
-- Table structure for table `product_stocks`
--

CREATE TABLE `product_stocks` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `variant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double(10,2) NOT NULL DEFAULT 0.00,
  `qty` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_stocks`
--

INSERT INTO `product_stocks` (`id`, `product_id`, `variant`, `sku`, `price`, `qty`, `created_at`, `updated_at`) VALUES
(1, 15, 'Amethyst-l', 'Y-Amethyst-l', 1000.00, 6, '2020-07-12 11:03:04', '2020-07-12 13:24:22'),
(2, 15, 'Amethyst-m', 'Y-Amethyst-m', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(3, 15, 'Amethyst-s', 'Y-Amethyst-s', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(4, 15, 'AntiqueWhite-l', 'Y-AntiqueWhite-l', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(5, 15, 'AntiqueWhite-m', 'Y-AntiqueWhite-m', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(6, 15, 'AntiqueWhite-s', 'Y-AntiqueWhite-s', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(7, 15, 'Aquamarine-l', 'Y-Aquamarine-l', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(8, 15, 'Aquamarine-m', 'Y-Aquamarine-m', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(9, 15, 'Aquamarine-s', 'Y-Aquamarine-s', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(774, 24, 'Black-medium-40', 'SB8c(ILTU-Black-medium-40', 300000.00, 10, '2020-07-26 22:47:18', '2020-07-26 22:47:18'),
(775, 24, 'Black-medium-41', 'SB8c(ILTU-Black-medium-41', 300000.00, 10, '2020-07-26 22:47:18', '2020-07-26 22:47:18'),
(776, 24, 'Black-small-40', 'SB8c(ILTU-Black-small-40', 300000.00, 8, '2020-07-26 22:56:22', '2020-07-27 18:24:03'),
(777, 24, 'Black-small-41', 'SB8c(ILTU-Black-small-41', 300000.00, 10, '2020-07-26 22:56:22', '2020-07-26 22:56:22'),
(778, 24, 'Black-large-40', 'SB8c(ILTU-Black-large-40', 300000.00, 10, '2020-07-26 22:56:22', '2020-07-26 22:56:22'),
(779, 24, 'Black-large-41', 'SB8c(ILTU-Black-large-41', 300000.00, 10, '2020-07-26 22:56:22', '2020-07-26 22:56:22'),
(784, 23, 'Black-40-large', 'SBK4LST-Black-40-large', 30000.00, 10, '2020-07-26 22:59:52', '2020-07-26 22:59:52'),
(785, 23, 'Black-41-large', 'SBK4LST-Black-41-large', 30000.00, 10, '2020-07-26 22:59:52', '2020-07-26 22:59:52'),
(786, 22, 'Black-small-silk', 'ATLB-AHWBwUCPf1LB-Black-small-silk', 3500.00, 10, '2020-07-26 23:01:51', '2020-07-26 23:01:51'),
(787, 22, 'Black-large-silk', 'ATLB-AHWBwUCPf1LB-Black-large-silk', 3500.00, 10, '2020-07-26 23:01:51', '2020-07-26 23:01:51'),
(788, 22, 'Blue-small-silk', 'ATLB-AHWBwUCPf1LB-Blue-small-silk', 3500.00, 10, '2020-07-26 23:01:51', '2020-07-26 23:01:51'),
(789, 22, 'Blue-large-silk', 'ATLB-AHWBwUCPf1LB-Blue-large-silk', 3500.00, 10, '2020-07-26 23:01:51', '2020-07-26 23:01:51'),
(790, 22, 'LightGrey-small-silk', 'ATLB-AHWBwUCPf1LB-LightGrey-small-silk', 3500.00, 10, '2020-07-26 23:01:51', '2020-07-26 23:01:51'),
(791, 22, 'LightGrey-large-silk', 'ATLB-AHWBwUCPf1LB-LightGrey-large-silk', 3500.00, 10, '2020-07-26 23:01:51', '2020-07-26 23:01:51'),
(792, 21, 'SaddleBrown-small-cotton', 'PZJKT-SaddleBrown-small-cotton', 1000.00, 10, '2020-07-26 23:02:25', '2020-07-26 23:02:25'),
(793, 21, 'SaddleBrown-medium-cotton', 'PZJKT-SaddleBrown-medium-cotton', 1000.00, 10, '2020-07-26 23:02:25', '2020-07-26 23:02:25'),
(794, 21, 'SaddleBrown-large-cotton', 'PZJKT-SaddleBrown-large-cotton', 1000.00, 10, '2020-07-26 23:02:25', '2020-07-26 23:02:25'),
(795, 20, 'Brown-small-500gm', 'BLGFSFM(-Brown-small-500gm', 4000.00, 10, '2020-07-26 23:03:13', '2020-07-26 23:03:13'),
(796, 20, 'Brown-large-500gm', 'BLGFSFM(-Brown-large-500gm', 4000.00, 10, '2020-07-26 23:03:13', '2020-07-26 23:03:13'),
(797, 19, 'Brown-small-500gmandmore', 'RSBLULBFM(-Brown-small-500gmandmore', 3000.00, 10, '2020-07-26 23:03:44', '2020-07-26 23:03:44'),
(798, 19, 'Brown-large-500gmandmore', 'RSBLULBFM(-Brown-large-500gmandmore', 3000.00, 9, '2020-07-26 23:03:44', '2020-07-30 16:50:44'),
(799, 18, 'AliceBlue-1gb-300gm-40', 'SGM-AliceBlue-1gb-300gm-40', 20000.00, 10, '2020-07-26 23:04:58', '2020-07-26 23:04:58'),
(800, 18, 'AliceBlue-1gb-300gm-41', 'SGM-AliceBlue-1gb-300gm-41', 20000.00, 10, '2020-07-26 23:04:58', '2020-07-26 23:04:58'),
(801, 18, 'AliceBlue-4gb-300gm-40', 'SGM-AliceBlue-4gb-300gm-40', 20000.00, 10, '2020-07-26 23:04:58', '2020-07-26 23:04:58'),
(802, 18, 'AliceBlue-4gb-300gm-41', 'SGM-AliceBlue-4gb-300gm-41', 20000.00, 10, '2020-07-26 23:04:58', '2020-07-26 23:04:58'),
(803, 18, 'AliceBlue-8gb-300gm-40', 'SGM-AliceBlue-8gb-300gm-40', 20000.00, 10, '2020-07-26 23:04:58', '2020-07-26 23:04:58'),
(804, 18, 'AliceBlue-8gb-300gm-41', 'SGM-AliceBlue-8gb-300gm-41', 20000.00, 10, '2020-07-26 23:04:58', '2020-07-26 23:04:58'),
(805, 18, 'Black-1gb-300gm-40', 'SGM-Black-1gb-300gm-40', 20000.00, 10, '2020-07-26 23:04:58', '2020-07-26 23:04:58'),
(806, 18, 'Black-1gb-300gm-41', 'SGM-Black-1gb-300gm-41', 20000.00, 10, '2020-07-26 23:04:58', '2020-07-26 23:04:58'),
(807, 18, 'Black-4gb-300gm-40', 'SGM-Black-4gb-300gm-40', 20000.00, 10, '2020-07-26 23:04:58', '2020-07-26 23:04:58'),
(808, 18, 'Black-4gb-300gm-41', 'SGM-Black-4gb-300gm-41', 20000.00, 10, '2020-07-26 23:04:58', '2020-07-26 23:04:58'),
(809, 18, 'Black-8gb-300gm-40', 'SGM-Black-8gb-300gm-40', 20000.00, 9, '2020-07-26 23:04:58', '2020-07-27 18:00:12'),
(810, 18, 'Black-8gb-300gm-41', 'SGM-Black-8gb-300gm-41', 20000.00, 10, '2020-07-26 23:04:58', '2020-07-26 23:04:58'),
(811, 17, 'CornflowerBlue-small-cotton', 'BJVHWJFWBJLMS-CornflowerBlue-small-cotton', 2000.00, 10, '2020-07-26 23:05:46', '2020-07-26 23:05:46'),
(812, 17, 'CornflowerBlue-small-Rayon', 'BJVHWJFWBJLMS-CornflowerBlue-small-Rayon', 2000.00, 10, '2020-07-26 23:05:46', '2020-07-26 23:05:46'),
(813, 17, 'CornflowerBlue-medium-cotton', 'BJVHWJFWBJLMS-CornflowerBlue-medium-cotton', 2000.00, 10, '2020-07-26 23:05:46', '2020-07-26 23:05:46'),
(814, 17, 'CornflowerBlue-medium-Rayon', 'BJVHWJFWBJLMS-CornflowerBlue-medium-Rayon', 2000.00, 10, '2020-07-26 23:05:46', '2020-07-26 23:05:46'),
(815, 17, 'CornflowerBlue-large-cotton', 'BJVHWJFWBJLMS-CornflowerBlue-large-cotton', 2000.00, 10, '2020-07-26 23:05:46', '2020-07-26 23:05:46'),
(816, 17, 'CornflowerBlue-large-Rayon', 'BJVHWJFWBJLMS-CornflowerBlue-large-Rayon', 2000.00, 10, '2020-07-26 23:05:46', '2020-07-26 23:05:46'),
(817, 16, 'Brown-small-silk-500gm', 's-Brown-small-silk-500gm', 2400.00, 10, '2020-07-26 23:09:52', '2020-07-26 23:09:52'),
(818, 16, 'Brown-small-Rayon-500gm', 's-Brown-small-Rayon-500gm', 2400.00, 10, '2020-07-26 23:09:52', '2020-07-26 23:09:52'),
(819, 16, 'Brown-medium-silk-500gm', 's-Brown-medium-silk-500gm', 2400.00, 10, '2020-07-26 23:09:52', '2020-07-26 23:09:52'),
(820, 16, 'Brown-medium-Rayon-500gm', 's-Brown-medium-Rayon-500gm', 2400.00, 10, '2020-07-26 23:09:52', '2020-07-26 23:09:52'),
(821, 16, 'Brown-large-silk-500gm', 's-Brown-large-silk-500gm', 2400.00, 10, '2020-07-26 23:09:52', '2020-07-26 23:09:52'),
(822, 16, 'Brown-large-Rayon-500gm', 's-Brown-large-Rayon-500gm', 2400.00, 10, '2020-07-26 23:09:52', '2020-07-26 23:09:52'),
(827, 25, 'Blue', 'BQSIPTB5PWTCAPW-Blue', 999.00, 10, '2020-07-30 19:31:19', '2020-07-30 19:31:19'),
(828, 26, 'XL', 'RAKwD-XL', 4000.00, 10, '2020-07-30 19:32:34', '2020-07-30 19:33:40'),
(829, 26, 'M', 'RAKwD-M', 3500.00, 8, '2020-07-30 19:36:36', '2020-07-30 19:44:16'),
(830, 26, 'XXL', 'RAKwD-XXL', 5000.00, 9, '2020-07-30 19:38:02', '2020-07-30 19:49:22'),
(831, 27, 'L-Cotton', 'RAKwD-L-Cotton', 3500.00, 10, '2020-07-30 19:58:03', '2020-07-30 19:58:03'),
(832, 28, 'L-Cotton', 'RAKwD-L-Cotton', 3500.00, 10, '2020-07-30 19:58:06', '2020-07-30 19:58:06'),
(833, 29, 'Black', 'BQSIPTB5PWTCAPW-Black', 2000.00, 10, '2020-07-30 20:01:45', '2020-07-30 20:01:45'),
(834, 29, 'DarkRed', 'BQSIPTB5PWTCAPW-DarkRed', 2000.00, 10, '2020-07-30 20:01:45', '2020-07-30 20:01:45'),
(835, 30, 'XL', 'RAKwD-XL', 3500.00, 1, '2020-07-30 20:02:07', '2020-07-30 20:02:07'),
(836, 31, 'XL', 'RAKwD-XL', 3500.00, 1, '2020-07-30 20:05:04', '2020-07-30 20:05:04'),
(837, 32, 'XXL', 'CKwP-XXL', 2740.00, 1, '2020-07-30 20:07:52', '2020-07-30 20:07:52'),
(838, 33, 'XL', 'WMaLGCSKWE-XL', 4000.00, 10, '2020-07-30 20:11:59', '2020-07-30 20:11:59'),
(839, 34, 'XL', 'CEWK-XL', 2000.00, 10, '2020-07-30 20:15:43', '2020-07-30 20:15:43'),
(840, 35, 'BlueViolet-S', 'tp-BlueViolet-S', 121.00, 10, '2020-08-02 23:36:04', '2020-08-02 23:36:04'),
(841, 35, 'BlueViolet-M', 'tp-BlueViolet-M', 121.00, 10, '2020-08-02 23:36:04', '2020-08-02 23:36:04'),
(842, 35, 'Brown-S', 'tp-Brown-S', 121.00, 9, '2020-08-02 23:36:04', '2020-08-02 23:37:28'),
(843, 35, 'Brown-M', 'tp-Brown-M', 121.00, 10, '2020-08-02 23:36:04', '2020-08-02 23:36:04'),
(844, 36, 'Orange', 'CSS-Orange', 2800.00, 10, '2020-08-04 15:59:40', '2020-08-04 15:59:40'),
(845, 36, 'Red', 'CSS-Red', 2800.00, 10, '2020-08-04 15:59:40', '2020-08-04 15:59:40'),
(846, 36, 'SeaGreen', 'CSS-SeaGreen', 2800.00, 10, '2020-08-04 15:59:40', '2020-08-04 15:59:40'),
(847, 38, 'Black-XS', 'BQSIPTB5PWTCAPW-Black-XS', 2000.00, 10, '2020-08-05 20:21:11', '2020-08-05 20:21:11'),
(848, 38, 'Black-S', 'BQSIPTB5PWTCAPW-Black-S', 2000.00, 10, '2020-08-05 20:21:11', '2020-08-05 20:21:11'),
(849, 38, 'Black-M', 'BQSIPTB5PWTCAPW-Black-M', 2000.00, 10, '2020-08-05 20:21:11', '2020-08-05 20:21:11'),
(850, 38, 'Black-L', 'BQSIPTB5PWTCAPW-Black-L', 2000.00, 10, '2020-08-12 23:18:30', '2020-08-12 23:18:30'),
(851, 38, 'Black-XXL', 'BQSIPTB5PWTCAPW-Black-XXL', 2000.00, 10, '2020-08-12 23:18:30', '2020-08-12 23:18:30'),
(852, 39, 'Black-5\'\'-6GB', 'SGSP-Black-5\'\'-6GB', 120000.00, 10, '2020-08-17 17:22:50', '2020-08-17 17:22:50'),
(853, 40, 'White-L', 'FST-fW-White-L', 1200.00, 10, '2020-08-17 17:35:51', '2020-08-17 17:35:51'),
(854, 41, 'Coral-8GB-5\'\'', 'M1[R/2R-1QCwO-Coral-8GB-5\'\'', 89999.00, 1, '2020-08-17 17:48:25', '2020-08-17 17:48:25'),
(855, 41, 'LightGrey-8GB-5\'\'', 'M1[R/2R-1QCwO-LightGrey-8GB-5\'\'', 89999.00, 1, '2020-08-17 17:48:25', '2020-08-17 17:48:25'),
(856, 42, 'Black-64GB', 'AiS-Black-64GB', 69900.00, 1, '2020-08-17 17:59:47', '2020-08-17 17:59:47'),
(857, 42, 'White-64GB', 'AiS-White-64GB', 69900.00, 1, '2020-08-17 17:59:47', '2020-08-17 17:59:47'),
(858, 43, 'Black', 'N1D(E-2FP(RDS-Black', 1899.00, 1, '2020-08-17 18:06:18', '2020-08-17 18:06:18'),
(859, 43, 'HotPink', 'N1D(E-2FP(RDS-HotPink', 1899.00, 1, '2020-08-17 18:06:18', '2020-08-17 18:06:18'),
(860, 44, 'DarkRed', 'VY([33R(&F8-DarkRed', 18590.00, 1, '2020-08-17 18:13:40', '2020-08-17 18:13:40'),
(861, 44, 'LightGrey', 'VY([33R(&F8-LightGrey', 18590.00, 1, '2020-08-17 18:13:40', '2020-08-17 18:13:40'),
(862, 45, 'Black-6GB-5\'\'', 'HY[6GR1GR]6iDpSCF(-Black-6GB-5\'\'', 33900.00, 1, '2020-08-17 18:21:43', '2020-08-17 18:21:43'),
(863, 45, 'Aqua-6GB-5\'\'', 'HY[6GR1GR]6iDpSCF(-Aqua-6GB-5\'\'', 33900.00, 1, '2020-08-17 18:21:43', '2020-08-17 18:21:43'),
(864, 46, 'Black-8GB-5\'\'', 'SGmRR-Black-8GB-5\'\'', 35699.00, 1, '2020-08-17 18:34:07', '2020-08-17 18:34:07'),
(865, 46, 'Blue-8GB-5\'\'', 'SGmRR-Blue-8GB-5\'\'', 35699.00, 1, '2020-08-17 18:34:07', '2020-08-17 18:34:07'),
(866, 47, 'Black', 'LLTi9GRHR2D-Black', 289990.00, 1, '2020-08-17 18:40:01', '2020-08-17 18:40:01'),
(867, 48, 'Black-8GB', 'DGIG8RHSNG1G1-Black-8GB', 122000.00, 1, '2020-08-17 18:48:49', '2020-08-17 18:48:49'),
(868, 48, 'Red-8GB', 'DGIG8RHSNG1G1-Red-8GB', 122000.00, 1, '2020-08-17 18:48:49', '2020-08-17 18:48:49'),
(869, 49, 'LightBlue-2GB-5\'\'', 'Li4F5HD5MFC5MRC3GR1RAPB21QC-LightBlue-2GB-5\'\'', 640.00, 10, '2020-08-18 16:45:38', '2020-08-18 16:45:38'),
(870, 50, 'Gold-4\'\'-2GB', 'MMESMP[R1R-Gold-4\'\'-2GB', 9999.00, 10, '2020-08-18 16:55:00', '2020-08-18 16:55:00'),
(871, 50, 'LightGrey-4\'\'-2GB', 'MMESMP[R1R-LightGrey-4\'\'-2GB', 9999.00, 10, '2020-08-18 16:55:00', '2020-08-18 16:55:00'),
(872, 51, 'Silver-5\'\'-8GB', 'M1[R/2R-1QCwO-Silver-5\'\'-8GB', 90999.00, 10, '2020-08-18 17:09:24', '2020-08-18 17:09:24'),
(873, 52, 'Silver-2GB-5\'\'', 'OA4-Silver-2GB-5\'\'', 16490.00, 10, '2020-08-18 17:21:11', '2020-08-18 17:21:11'),
(874, 53, 'Red-4GB-4\'\'', 'AiS(Qwc-Red-4GB-4\'\'', 68500.00, 10, '2020-08-18 17:27:56', '2020-08-18 17:27:56'),
(875, 54, 'Gold', 'Ai1PM-Gold', 175000.00, 10, '2020-08-18 17:50:43', '2020-08-18 17:50:43'),
(876, 54, 'MediumSeaGreen', 'Ai1PM-MediumSeaGreen', 175000.00, 10, '2020-08-18 17:50:43', '2020-08-18 17:50:43'),
(877, 55, 'Black-8GB', 'VV-R/1GR-Black-8GB', 49999.00, 10, '2020-08-18 17:58:49', '2020-08-18 17:58:49'),
(878, 55, 'Silver-8GB', 'VV-R/1GR-Silver-8GB', 49999.00, 10, '2020-08-18 17:58:49', '2020-08-18 17:58:49'),
(879, 56, 'CornflowerBlue-6GB', 'VVP(-CornflowerBlue-6GB', 50390.00, 10, '2020-08-18 18:03:26', '2020-08-18 18:03:26'),
(880, 56, 'DarkRed-6GB', 'VVP(-DarkRed-6GB', 50390.00, 10, '2020-08-18 18:03:26', '2020-08-18 18:03:26'),
(881, 57, 'Aqua-4GB', 'VY3R3R-Aqua-4GB', 19490.00, 10, '2020-08-18 18:22:41', '2020-08-18 18:22:41'),
(882, 57, 'DarkRed-4GB', 'VY3R3R-DarkRed-4GB', 19490.00, 10, '2020-08-18 18:22:41', '2020-08-18 18:22:41'),
(883, 58, 'Black-4GB', 'VY(R/6R-Black-4GB', 24999.00, 10, '2020-08-18 18:38:10', '2020-08-18 18:38:10'),
(884, 58, 'White-4GB', 'VY(R/6R-White-4GB', 24999.00, 10, '2020-08-18 18:38:10', '2020-08-18 18:38:10'),
(885, 59, 'Aqua-4GB', 'VY[RMC(S-Aqua-4GB', 21590.00, 10, '2020-08-18 18:44:44', '2020-08-18 18:44:44'),
(886, 60, 'Aqua-4GB', 'VY[RMC(S-Aqua-4GB', 21590.00, 10, '2020-08-18 18:44:47', '2020-08-18 18:44:47'),
(887, 61, 'Black-2GB', 'VY262R3R1R5FC-Black-2GB', 15590.00, 10, '2020-08-18 18:55:11', '2020-08-18 18:55:11'),
(888, 61, 'Blue-2GB', 'VY262R3R1R5FC-Blue-2GB', 15590.00, 10, '2020-08-18 18:55:11', '2020-08-18 18:55:11'),
(889, 62, 'Black-2GB', 'VY262R3R1R5FC-Black-2GB', 15590.00, 10, '2020-08-18 18:55:13', '2020-08-18 18:55:13'),
(890, 62, 'Blue-2GB', 'VY262R3R1R5FC-Blue-2GB', 15590.00, 10, '2020-08-18 18:55:13', '2020-08-18 18:55:13'),
(891, 63, 'Pink-XL', 'RAKwD-Pink-XL', 3500.00, 10, '2020-08-18 19:38:23', '2020-08-18 19:38:23'),
(892, 64, 'Pink-XL', 'RAKwD-Pink-XL', 3500.00, 10, '2020-08-18 19:38:25', '2020-08-18 19:38:25'),
(893, 65, 'Pink-L', 'RAKwD-Pink-L', 3500.00, 10, '2020-08-18 19:45:22', '2020-08-18 19:45:22'),
(894, 66, 'Red-XL', 'RAKwD-Red-XL', 3500.00, 10, '2020-08-18 19:49:58', '2020-08-18 19:53:30'),
(895, 64, 'SeaGreen-XL', 'RAKwD-SeaGreen-XL', 3500.00, 10, '2020-08-18 19:52:36', '2020-08-18 19:52:36'),
(896, 67, 'Red-XL', 'RAKwD-Red-XL', 3500.00, 10, '2020-08-18 20:00:30', '2020-08-18 20:00:30'),
(897, 68, 'DarkBlue-XXL', 'CKwP-DarkBlue-XXL', 2740.00, 10, '2020-08-18 20:09:33', '2020-08-18 20:09:33'),
(898, 69, 'LightGreen-XL', 'WMaLGCSKWE-LightGreen-XL', 4000.00, 10, '2020-08-18 20:14:39', '2020-08-18 20:14:39'),
(899, 70, 'DarkSeaGreen-XL', 'CEWK-DarkSeaGreen-XL', 2000.00, 10, '2020-08-18 20:20:18', '2020-08-18 20:20:18'),
(900, 71, 'Navy-XL', 'CEWK(B-Navy-XL', 2000.00, 10, '2020-08-18 20:24:00', '2020-08-18 20:24:00'),
(901, 72, 'Black-XL', 'CEWK(-Black-XL', 2000.00, 10, '2020-08-18 20:27:27', '2020-08-18 20:27:27'),
(902, 73, 'Black-XL', 'CEWK(-Black-XL', 2000.00, 10, '2020-08-18 20:27:29', '2020-08-18 20:27:29'),
(903, 74, 'White-XL', 'CEWK(-White-XL', 2000.00, 10, '2020-08-18 20:32:35', '2020-08-18 20:32:35'),
(904, 75, 'DarkGreen-XL', 'CEWK(G-DarkGreen-XL', 2000.00, 10, '2020-08-18 20:37:31', '2020-08-18 20:37:31'),
(905, 76, 'LightGreen-XL', 'CEWK(G-LightGreen-XL', 2000.00, 10, '2020-08-18 20:41:29', '2020-08-18 20:41:29'),
(906, 77, 'DarkRed-XL', 'CEWK(B-DarkRed-XL', 2000.00, 10, '2020-08-18 20:45:31', '2020-08-18 20:45:31'),
(907, 78, 'SkyBlue-XXL', 'CEWK(B-SkyBlue-XXL', 2000.00, 10, '2020-08-18 20:49:47', '2020-08-18 20:49:47'),
(908, 79, 'CornflowerBlue-XL', '5000121', 3500.00, 10, '2020-08-18 22:04:40', '2020-08-18 22:55:17'),
(909, 80, 'Pink-L', '5000124', 3500.00, 10, '2020-08-18 22:08:10', '2020-08-18 22:54:31'),
(910, 81, 'DarkSeaGreen-XL', '5000129', 3500.00, 10, '2020-08-18 22:15:31', '2020-08-18 23:14:46'),
(911, 82, 'IndianRed-XL', '5000129', 3500.00, 10, '2020-08-18 22:23:45', '2020-08-18 22:52:24'),
(912, 83, 'Black-6GB-5\'\'', 'SGM[R/1R-Black-6GB-5\'\'', 29999.01, 10, '2020-08-18 22:45:28', '2020-08-18 22:45:28'),
(913, 83, 'BlueViolet-6GB-5\'\'', 'SGM[R/1R-BlueViolet-6GB-5\'\'', 29999.01, 10, '2020-08-18 22:45:28', '2020-08-18 22:45:28'),
(914, 84, 'Blue-XXL', '5000149', 2740.00, 10, '2020-08-18 22:47:18', '2020-08-18 22:47:18'),
(915, 85, 'DarkSeaGreen-XL', '5000185', 4000.00, 10, '2020-08-18 22:59:05', '2020-08-18 23:11:28'),
(916, 86, 'LightGreen-XL', '5000107', 2000.00, 10, '2020-08-18 23:02:52', '2020-08-18 23:10:51'),
(917, 87, 'DarkBlue-XL', '5000108', 2000.00, 10, '2020-08-18 23:05:49', '2020-08-18 23:10:20'),
(918, 88, 'DarkGreen-XL', '5000109', 2000.00, 10, '2020-08-18 23:09:49', '2020-08-18 23:09:49'),
(919, 89, 'White-XL', 'CEWK(-White-XL', 2000.00, 10, '2020-08-18 23:20:00', '2020-08-18 23:20:00'),
(920, 90, 'DarkGreen-XL', 'CEWK(G-DarkGreen-XL', 2000.00, 10, '2020-08-18 23:23:19', '2020-08-18 23:23:19');

-- --------------------------------------------------------

--
-- Table structure for table `refund_requests`
--

CREATE TABLE `refund_requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_detail_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `seller_approval` int(1) NOT NULL DEFAULT 0,
  `admin_approval` int(1) NOT NULL DEFAULT 0,
  `refund_amount` double(8,2) NOT NULL DEFAULT 0.00,
  `reason` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_seen` int(11) NOT NULL,
  `refund_status` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL DEFAULT 0,
  `comment` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `viewed` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Manager', '[\"1\",\"2\",\"4\"]', '2018-10-10 04:39:47', '2018-10-10 04:51:37'),
(2, 'Accountant', '[\"2\",\"3\"]', '2018-10-10 04:52:09', '2018-10-10 04:52:09');

-- --------------------------------------------------------

--
-- Table structure for table `searches`
--

CREATE TABLE `searches` (
  `id` int(11) NOT NULL,
  `query` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `searches`
--

INSERT INTO `searches` (`id`, `query`, `count`, `created_at`, `updated_at`) VALUES
(2, 'dcs', 1, '2020-03-08 00:29:09', '2020-03-08 00:29:09'),
(3, 'das', 3, '2020-03-08 00:29:15', '2020-03-08 00:29:50');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE `sellers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `verification_status` int(1) NOT NULL DEFAULT 0,
  `verification_info` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `cash_on_delivery_status` int(1) NOT NULL DEFAULT 0,
  `admin_to_pay` double(8,2) NOT NULL DEFAULT 0.00,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_acc_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_acc_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_routing_no` int(50) DEFAULT NULL,
  `bank_payment_status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`id`, `user_id`, `verification_status`, `verification_info`, `cash_on_delivery_status`, `admin_to_pay`, `bank_name`, `bank_acc_name`, `bank_acc_no`, `bank_routing_no`, `bank_payment_status`, `created_at`, `updated_at`) VALUES
(8, 26, 1, NULL, 0, 0.00, NULL, NULL, NULL, NULL, 0, '2020-07-17 12:47:02', '2020-07-17 19:26:29'),
(10, 28, 1, NULL, 1, 6300.00, 'Nmb Bank', 'Nepshopy', '235546875645896', 2315465, 0, '2020-07-30 17:50:52', '2020-07-31 16:07:38'),
(12, 28, 0, NULL, 0, 0.00, NULL, NULL, NULL, NULL, 0, '2020-07-31 17:08:34', '2020-07-31 17:08:34'),
(13, 33, 1, NULL, 1, 0.00, NULL, NULL, NULL, NULL, 0, '2020-08-18 21:56:46', '2020-08-18 22:30:49'),
(14, 34, 1, NULL, 0, 0.00, NULL, NULL, NULL, NULL, 0, '2020-08-18 22:33:50', '2020-08-18 22:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `seller_withdraw_requests`
--

CREATE TABLE `seller_withdraw_requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `message` longtext DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `viewed` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `seo_settings`
--

CREATE TABLE `seo_settings` (
  `id` int(11) NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `revisit` int(11) NOT NULL,
  `sitemap_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seo_settings`
--

INSERT INTO `seo_settings` (`id`, `keyword`, `author`, `revisit`, `sitemap_link`, `description`, `created_at`, `updated_at`) VALUES
(1, 'bootstrap,responsive,template,developer', 'Active IT Zone', 11, 'https://www.activeitzone.com', 'Active E-commerce CMS Multi vendor system is such a platform to build a border less marketplace both for physical and digital goods.', '2019-08-08 08:56:11', '2019-08-08 02:56:11');

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sliders` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `pick_up_point_id` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_cost` double(8,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `user_id`, `name`, `logo`, `sliders`, `address`, `facebook`, `google`, `twitter`, `youtube`, `slug`, `meta_title`, `meta_description`, `pick_up_point_id`, `shipping_cost`, `created_at`, `updated_at`) VALUES
(7, 26, 'asdadadad', NULL, '[\"uploads\\/shop\\/sliders\\/Xbpo2plIexeZXoyBedm93Rjw4OCiU6eD5HSc3G75.jpeg\"]', 'asasacaxzaxasdas', NULL, NULL, NULL, NULL, 'asdadadad-7', 'your shop', 'all at this shop', '[]', 50.00, '2020-07-17 12:47:02', '2020-07-17 12:48:11'),
(10, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'demo-shop-33', NULL, NULL, NULL, 0.00, '2020-08-18 21:56:46', '2020-08-18 21:56:46'),
(11, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'demo-shop-34', NULL, NULL, NULL, 0.00, '2020-08-18 22:33:50', '2020-08-18 22:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published` int(1) NOT NULL DEFAULT 1,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `photo`, `published`, `link`, `created_at`, `updated_at`) VALUES
(7, 'uploads/sliders/slider-image.jpg', 1, NULL, '2019-03-12 05:58:05', '2019-03-12 05:58:05'),
(8, 'uploads/sliders/slider-image.jpg', 1, NULL, '2019-03-12 05:58:12', '2019-03-12 05:58:12');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 13, 3, '2020-07-12 16:37:52', '2020-07-12 16:39:12'),
(2, 22, 3, '2020-07-16 22:08:31', '2020-07-16 22:08:31'),
(3, 31, 1, '2020-08-10 16:26:33', '2020-08-10 16:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `category_id`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 'Accessories', 1, 'Accessoriesq-cqnSt', 'accessories', 'All other accessories', '2019-03-12 06:13:24', '2020-07-12 16:21:08'),
(2, 'Traditional clothing', 1, 'Traditional-clothing-4W98I', 'traditionalclothing', 'all traditional clothing', '2019-03-12 06:13:44', '2020-07-12 16:20:22'),
(3, 'Clothing', 1, 'clothing-hOsSo', 'clothing', 'all clothing items', '2019-03-12 06:13:59', '2020-08-18 21:41:24'),
(4, 'Desktop', 3, 'Desktop-FeBmQ', 'desktops', 'all desktop items', '2019-03-12 06:18:25', '2020-07-12 16:18:06'),
(5, 'Shoes', 2, 'Shoes-lNQqv', 'shoes', 'all shoes items', '2019-03-12 06:18:38', '2020-07-12 16:17:38'),
(6, 'Clothing', 2, 'Clothing-dseah', 'clothing', 'all clothing items', '2019-03-12 06:18:51', '2020-07-12 16:17:12'),
(7, 'Laptops', 3, 'Laptops-AEAaW', 'laptop', 'all Laptop items', '2019-03-12 06:19:05', '2020-07-12 16:16:43'),
(8, 'Tablets', 3, 'Mobiles--Tablets-SmEJc', 'mobiles', 'all moibile items', '2019-03-12 06:19:13', '2020-07-30 22:37:50'),
(10, 'televisions', 4, 'televisions-4aZMU', 'tv', 'tvs', '2020-07-13 15:33:51', '2020-07-13 15:33:51'),
(11, 'Mobile', 3, 'Mobile-ZMEAm', 'mobiles', 'allkinds of mobiles', '2020-07-30 22:38:13', '2020-07-30 22:38:13'),
(12, 'Gaming Consoles', 3, 'Gaming-Consoles-WGK1O', 'gaming consoles', 'All gaming consoles', '2020-07-30 22:39:00', '2020-07-30 22:39:18'),
(13, 'Cameras', 3, 'Cameras-SjS6q', 'cameras', 'all cameras', '2020-07-30 22:39:44', '2020-07-30 22:39:44'),
(14, 'Printers', 3, 'Printers-u5Ug9', 'Printers', 'All printers', '2020-07-30 22:40:14', '2020-07-30 22:40:14'),
(15, 'Mobile Accessories', 12, 'Mobile-Accessories-3iP2D', 'Mobile Accessories', 'All Mobile Accessories', '2020-07-30 22:42:23', '2020-07-30 22:42:23'),
(16, 'Audio', 12, 'Audio-SiUaQ', 'audio', 'all Audio types', '2020-07-30 22:42:51', '2020-07-30 22:42:51'),
(17, 'wearable', 12, 'wearable-JIVvc', 'wearable', 'all wearable Accessories', '2020-07-30 22:43:22', '2020-07-30 22:43:22'),
(18, 'Console Accessories', 12, 'Console-Accessories-pqeUy', 'Console Accessories', 'Console Accessories', '2020-07-30 22:44:10', '2020-07-30 22:44:10'),
(19, 'Camera Accessories', 12, 'Camera-Accessories-Gv8Op', 'Camera Accessories', 'All Camera Accessories', '2020-07-30 22:44:36', '2020-07-30 22:44:36'),
(20, 'Computer Accessories', 12, 'Computer-Accessories-KkrVC', 'Computer Accessories', 'All Computer Accessories', '2020-07-30 22:45:03', '2020-07-30 22:45:03'),
(21, 'Storage', 12, 'Storage-KltcV', 'Storage', 'All Storage', '2020-07-30 22:45:24', '2020-07-30 22:45:24'),
(22, 'Computer Components', 12, 'Computer-Components-JRWTU', 'Computer Components', 'All Computer Components', '2020-07-30 22:45:49', '2020-07-30 22:45:49'),
(23, 'Network Components', 12, 'Network-Components-GG8EJ', 'Network Components', 'Network Components', '2020-07-30 22:46:13', '2020-07-30 22:46:13'),
(24, 'Women\'s Bags', 1, 'Womens-Bags-wUO56', 'Women\'s Bags', 'All Women\'s Bags', '2020-07-30 22:50:30', '2020-07-30 22:50:30'),
(25, 'Shoes', 1, 'Shoes-gepkD', 'Shoes', 'All Shoes', '2020-07-30 22:50:43', '2020-07-30 22:50:43'),
(26, 'Lingerie, Sleep & Lounge', 1, 'Lingerie-Sleep--Lounge-4WDD0', 'Lingerie, Sleep & Lounge', 'All Lingerie, Sleep & Lounge', '2020-07-30 22:51:00', '2020-07-30 22:51:00'),
(27, 'Girl\'s Fashion', 1, 'Girls-Fashion-5Iswe', 'Girl\'s Fashion', 'All othert Girl\'s Fashion', '2020-07-30 22:51:37', '2020-07-30 22:51:37');

-- --------------------------------------------------------

--
-- Table structure for table `sub_sub_categories`
--

CREATE TABLE `sub_sub_categories` (
  `id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `attribute_id` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_sub_categories`
--

INSERT INTO `sub_sub_categories` (`id`, `sub_category_id`, `attribute_id`, `name`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(3, 1, NULL, 'Sandals', 'Sandals-rrXbH', 'sandals', 'all sandals items', '2019-03-12 06:20:43', '2020-07-12 16:46:26'),
(4, 3, NULL, 'Woolen Clothing', 'Woolen-Clothing-UpC0o', 'Woolen Clothing', 'all Woolen Clothingitems', '2019-03-12 06:21:28', '2020-07-12 16:46:48'),
(5, 3, NULL, 'plazo', 'plazo-kvvws', 'plazo', 'allplazo items', '2019-03-12 06:21:40', '2020-07-12 16:45:12'),
(6, 3, NULL, '3 piece', '3-piece-hAhuQ', '3piece', 'all 3 piece items', '2019-03-12 06:21:56', '2020-07-12 16:44:42'),
(7, 3, NULL, 'Pants & Leggings', 'Pant-pzVuj', 'pant', 'all pants', '2019-03-12 06:23:31', '2020-07-30 22:55:36'),
(8, 3, NULL, 'Gown', 'Gown-doW2x', 'gown', 'all gown items', '2019-03-12 06:23:48', '2020-07-12 16:43:15'),
(10, 5, NULL, 'formal shoes', 'formal-shoes-1N7co', 'formal', 'All formal shoes', '2019-03-12 06:24:37', '2020-07-12 16:49:23'),
(11, 4, NULL, 'Normal / Office', 'Normal--Office-E2CzO', 'normal', 'All normal and office use desktops', '2019-03-12 06:25:14', '2020-07-12 16:48:51'),
(12, 4, NULL, 'Gaming', 'Gaming-GcBdE', 'gamingh', 'all gaming desktops', '2019-03-12 06:25:25', '2020-07-12 16:48:05'),
(13, 5, NULL, 'sneakers', 'sneakers-oeIIc', 'sneakers', 'all sneakers items', '2019-03-12 06:25:58', '2020-07-12 16:47:30'),
(14, 6, NULL, 'casual wear', 'casual-wear-XoC6b', 'casualwear', 'all casual wears', '2019-03-12 06:26:16', '2020-07-12 16:35:00'),
(15, 7, NULL, 'gaming', 'gaming-leIOt', 'gaming', 'all gaming laptops', '2019-03-12 06:27:17', '2020-07-12 16:34:22'),
(16, 8, NULL, 'Tablets', 'Tablets-iBApl', 'tablet', 'all tablets', '2019-03-12 06:27:29', '2020-07-12 16:33:43'),
(17, 4, NULL, 'Gaming', 'Gaming-OM7SZ', 'gaming', 'all gaming desktops', '2019-03-12 06:27:41', '2020-07-12 16:25:11'),
(18, 10, NULL, 'led', 'led-scpCm', 'led tvs', 'led tvs', '2020-07-13 15:34:14', '2020-07-13 15:34:14'),
(19, 3, NULL, 'Tops & T-shirts', 'Tops--T-shirts-54Df8', 'Tops & T-shirts', 'Tops & T-shirts', '2020-07-30 22:53:42', '2020-07-30 22:53:42'),
(20, 3, NULL, 'Hoodies & Sweatshirts', 'Hoodies--Sweatshirts-OOWXY', 'Hoodies & Sweatshirts', 'Hoodies & Sweatshirts', '2020-07-30 22:53:53', '2020-07-30 22:53:53'),
(21, 3, NULL, 'Sweaters & Cardigans', 'Sweaters--Cardigans-6JUxk', 'Sweaters & Cardigans', 'Sweaters & Cardigans', '2020-07-30 22:54:05', '2020-07-30 22:54:05'),
(22, 3, NULL, 'Jackets & Coats', 'Jackets--Coats-a5rbD', 'Jackets & Coats', 'Jackets & Coats', '2020-07-30 22:54:16', '2020-07-30 22:54:16'),
(23, 3, NULL, 'Jeans', 'Jeans-jnhbG', 'Jeans', 'Jeans', '2020-07-30 22:55:49', '2020-07-30 22:55:49'),
(24, 3, NULL, 'Shorts', 'Shorts-70rWU', 'Shorts', 'Shorts', '2020-07-30 22:56:00', '2020-07-30 22:56:00'),
(25, 3, NULL, 'Skirts', 'Skirts-na8Ip', 'Skirts', 'Skirts', '2020-07-30 22:56:11', '2020-07-30 22:56:11'),
(26, 3, NULL, 'Dresses', 'Dresses-hNYzu', 'Dresses', 'Dresses', '2020-07-30 22:56:25', '2020-07-30 22:56:25'),
(27, 3, NULL, 'Party Wear', 'Party-Wear-9iwxu', 'Party Wear', 'Party Wear', '2020-07-30 22:56:39', '2020-07-30 22:56:39'),
(28, 2, NULL, 'Saree', 'Saree-B9TZD', 'Saree', 'Saree', '2020-07-30 22:57:34', '2020-07-30 22:57:34'),
(29, 2, NULL, 'Kurtas', 'Kurtas-oFBPU', 'Kurtas', 'Kurtas', '2020-07-30 22:57:45', '2020-07-30 22:57:45'),
(30, 2, NULL, 'Kurtis', 'Kurtis-d2ng8', 'Kurtis', 'Kurtis', '2020-07-30 22:57:58', '2020-07-30 22:57:58'),
(31, 2, NULL, 'Lehenga', 'Lehenga-s92y0', 'Lehenga', 'Lehenga', '2020-07-30 22:58:07', '2020-07-30 22:58:07'),
(32, 2, NULL, 'Unstitched Fabric', 'Unstitched-Fabric-zwOpR', 'Unstitched Fabric', 'Unstitched Fabric', '2020-07-30 22:58:18', '2020-07-30 22:58:18'),
(33, 2, NULL, 'Dupattas, Stoles & Shawls', 'Dupattas-Stoles--Shawls-KU2A8', 'Dupattas, Stoles & Shawls', 'Dupattas, Stoles & Shawls', '2020-07-30 22:58:28', '2020-07-30 22:58:28'),
(34, 24, NULL, 'Backpacks', 'Backpacks-VAFPs', 'Backpacks', 'Backpacks', '2020-07-30 23:01:01', '2020-07-30 23:01:01'),
(35, 24, NULL, 'Crossbody Bags', 'Crossbody-Bags-crVwP', 'Crossbody Bags', 'Crossbody Bags', '2020-07-30 23:01:17', '2020-07-30 23:01:17'),
(36, 24, NULL, 'Wallets & Cardholders', 'Wallets--Cardholders-783S5', 'Wallets & Cardholders', 'Wallets & Cardholders', '2020-07-30 23:01:29', '2020-07-30 23:01:29'),
(37, 24, NULL, 'Clutches', 'Clutches-ZMors', 'Clutches', 'Clutches', '2020-07-30 23:01:41', '2020-07-30 23:01:41'),
(38, 24, NULL, 'Top-handle Bags', 'Top-handle-Bags-p7B2s', 'Top-handle Bags', 'Top-handle Bags', '2020-07-30 23:01:51', '2020-07-30 23:01:51'),
(39, 25, NULL, 'Sneakers', 'Sneakers-b37vH', 'Sneakers', 'Sneakers', '2020-07-30 23:02:24', '2020-07-30 23:02:24'),
(40, 25, NULL, 'Boots', 'Boots-74V9F', 'Boots', 'Boots', '2020-07-30 23:02:34', '2020-07-30 23:02:34'),
(41, 25, NULL, 'Flat Shoes', 'Flat-Shoes-U5kpA', 'Flat Shoes', 'Flat Shoes', '2020-07-30 23:02:47', '2020-07-30 23:02:47'),
(42, 25, NULL, 'Heels', 'Heels-tvbhL', 'Heels', 'Heels', '2020-07-30 23:02:59', '2020-07-30 23:02:59'),
(43, 25, NULL, 'Sandals', 'Sandals-ft6Q5', 'Sandals', 'Sandals', '2020-07-30 23:03:08', '2020-07-30 23:03:08'),
(44, 25, NULL, 'Wedges', 'Wedges-xAr2l', 'Wedges', 'Wedges', '2020-07-30 23:03:17', '2020-07-30 23:03:17'),
(45, 1, NULL, 'Belts', 'Belts-tB9m9', 'Belts', 'Belts', '2020-07-30 23:03:42', '2020-07-30 23:03:42'),
(46, 1, NULL, 'Scarves & Mufflers', 'Scarves--Mufflers-7TMvk', 'Scarves & Mufflers', 'Scarves & Mufflers', '2020-07-30 23:03:51', '2020-07-30 23:03:51'),
(47, 1, NULL, 'Gloves', 'Gloves-kHIK5', 'Gloves', 'Gloves', '2020-07-30 23:04:00', '2020-07-30 23:04:00'),
(48, 1, NULL, 'Hats & Caps', 'Hats--Caps-O4pTJ', 'Hats & Caps', 'Hats & Caps', '2020-07-30 23:04:09', '2020-07-30 23:04:09'),
(49, 1, NULL, 'Hair Accessories', 'Hair-Accessories-vi9GU', 'Hair Accessories', 'Hair Accessories', '2020-07-30 23:04:20', '2020-07-30 23:04:20'),
(50, 1, NULL, 'Socks & Tights', 'Socks--Tights-0roBY', 'Socks & Tights', 'Socks & Tights', '2020-07-30 23:04:30', '2020-07-30 23:04:30'),
(51, 26, NULL, 'Bras', 'Bras-e2J5N', 'Bras', 'Bras', '2020-07-30 23:04:52', '2020-07-30 23:04:52'),
(52, 26, NULL, 'Lingerie Sets', 'Lingerie-Sets-C3Dgp', 'Lingerie Sets', 'Lingerie Sets', '2020-07-30 23:05:04', '2020-07-30 23:05:04'),
(53, 26, NULL, 'Panties', 'Panties-SitGA', 'Panties', 'Panties', '2020-07-30 23:05:20', '2020-07-30 23:05:20'),
(54, 26, NULL, 'Sleep & Lounge Wear', 'Sleep--Lounge-Wear-0kQ1P', 'Sleep & Lounge Wear', 'Sleep & Lounge Wear', '2020-07-30 23:05:33', '2020-07-30 23:05:33'),
(55, 26, NULL, 'Robes', 'Robes-YL6sa', 'Robes', 'Robes', '2020-07-30 23:05:43', '2020-07-30 23:05:43'),
(56, 26, NULL, 'Shapewear', 'Shapewear-D0Iqu', 'Shapewear', 'Shapewear', '2020-07-30 23:05:51', '2020-07-30 23:05:51'),
(57, 26, NULL, 'Sexy Lingerie', 'Sexy-Lingerie-dpOCn', 'Sexy Lingerie', 'Sexy Lingerie', '2020-07-30 23:06:02', '2020-07-30 23:06:02'),
(58, 26, NULL, 'Accessories', 'Accessories-Zkb8u', 'Accessories', 'Accessories', '2020-07-30 23:06:49', '2020-07-30 23:06:49'),
(59, 1, '17,35', 'Girl\'s Clothing', 'Girls-Clothing-fwVNt', 'Girl\'s Clothing', 'Girl\'s Clothing', '2020-07-30 23:07:18', '2020-08-18 03:18:01'),
(60, 27, NULL, 'Girl\'s Shoes', 'Girls-Shoes-Ftna3', 'Girl\'s Shoes', 'Girl\'s Shoes', '2020-07-30 23:07:42', '2020-07-30 23:07:42'),
(61, 27, NULL, 'Girl\'s Accessories', 'Girls-Accessories-LxnEY', 'Girl\'s Accessories', 'Girl\'s Accessories', '2020-07-30 23:08:10', '2020-07-30 23:08:10'),
(62, 11, NULL, 'Samsung Mobile', 'Samsung-Mobile-7UTkJ', 'Samsung Mobile', 'Samsung Mobile', '2020-07-30 23:09:37', '2020-07-30 23:09:37'),
(63, 11, NULL, 'Xiaomi Mobile', 'Xiaomi-Mobile-ZYEoq', 'Xiaomi Mobile', 'Xiaomi Mobile', '2020-07-30 23:09:53', '2020-07-30 23:09:53'),
(64, 11, NULL, 'Oppo Mobile', 'Oppo-Mobile-Sa38b', 'Oppo Mobile', 'Oppo Mobile', '2020-07-30 23:10:09', '2020-07-30 23:10:09'),
(65, 11, NULL, 'Nokia Mobile', 'Nokia-Mobile-jGfhu', 'Nokia Mobile', 'Nokia Mobile', '2020-07-30 23:10:23', '2020-07-30 23:10:23'),
(66, 11, NULL, 'Apple Mobile', 'Apple-Mobile-aR6ZH', 'Apple Mobile', 'Apple Mobile', '2020-07-30 23:10:35', '2020-07-30 23:10:35'),
(67, 11, NULL, 'Vivo Mobile', 'Vivo-Mobile-KPdF9', 'Vivo Mobile', 'Vivo Mobile', '2020-07-30 23:10:48', '2020-07-30 23:10:48'),
(68, 11, NULL, 'Huawei Mobile', 'Huawei-Mobile-iXxTy', 'Huawei Mobile', 'Huawei Mobile', '2020-07-30 23:10:57', '2020-07-30 23:13:00'),
(69, 11, NULL, 'Lenovo Mobile', 'Lenovo-Mobile-qJf0A', 'Lenovo Mobile', 'Lenovo Mobile', '2020-07-30 23:11:21', '2020-07-30 23:11:21'),
(70, 11, NULL, 'HTC Mobile', 'HTC-Mobile-F0jms', 'HTC Mobile', 'HTC Mobile', '2020-07-30 23:13:28', '2020-07-30 23:13:28'),
(71, 11, NULL, 'Colors Mobile', 'Colors-Mobile-uHrTd', 'Colors Mobile', 'Colors Mobile', '2020-07-30 23:13:45', '2020-07-30 23:13:45'),
(72, 11, NULL, 'Lava Mobile', 'Lava-Mobile-iN9JR', 'Lava Mobile', 'Lava Mobile', '2020-07-30 23:14:02', '2020-07-30 23:14:02'),
(73, 11, NULL, 'Motorola Mobile', 'Motorola-Mobile-gj6wg', 'Motorola Mobile', 'Motorola Mobile', '2020-07-30 23:14:20', '2020-07-30 23:14:20');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `code` int(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `files` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `viewed` int(1) NOT NULL DEFAULT 0,
  `client_viewed` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_replies`
--

CREATE TABLE `ticket_replies` (
  `id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reply` longtext COLLATE utf8_unicode_ci NOT NULL,
  `files` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `referred_by` int(11) DEFAULT NULL,
  `provider_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'customer',
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_original` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` double(8,2) NOT NULL DEFAULT 0.00,
  `referral_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_package_id` int(11) DEFAULT NULL,
  `remaining_uploads` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `referred_by`, `provider_id`, `user_type`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `avatar`, `avatar_original`, `address`, `country`, `city`, `postal_code`, `phone`, `balance`, `referral_code`, `customer_package_id`, `remaining_uploads`, `created_at`, `updated_at`) VALUES
(8, NULL, NULL, 'customer', 'Mr. Customer', 'customer@example.com', '2018-12-11 18:00:00', '$2y$10$eUKRlkmm2TAug75cfGQ4i.WoUbcJ2uVPqUlVkox.cv4CCyGEIMQEm', 'zileondgipAvHWatKgj3dVXfGxYDpjnMHVpXAj6liQkTffYHuMy8ja5p1T63', 'https://lh3.googleusercontent.com/-7OnRtLyua5Q/AAAAAAAAAAI/AAAAAAAADRk/VqWKMl4f8CI/photo.jpg?sz=50', 'uploads/ucQhvfz4EQXNeTbN8Eif0Cpq5LnOwvg8t7qKNKVs.jpeg', 'Demo address', 'US', 'Demo city', '1234', NULL, 0.00, '8zJTyXTlTT', NULL, NULL, '2018-10-07 04:42:57', '2020-03-03 04:26:11'),
(12, NULL, NULL, 'admin', 'admin', 'admin@admin.com', '2020-07-12 10:07:23', '$2y$10$9UfJw/fYyLLV0lFqRHhHe.xsFwbD5xoTNF5se8aBZtrotbfH6zt76', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, 0, '2020-07-12 10:54:23', '2020-07-12 10:54:23'),
(13, NULL, NULL, 'staff', 'Sunil Singh', 'nepshopytest@gmail.com', '2020-07-13 17:56:31', '$2y$10$WNjw80fqZx28tYl8R8TM0e/WhRW1sgtdBhzgq5.D0XTEPil6KnYD.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9841481154', 0.00, NULL, NULL, 0, '2020-07-12 16:37:52', '2020-07-12 16:41:46'),
(22, NULL, NULL, 'staff', 'Sonu', 'sonu@gmail.com', NULL, '$2y$10$UPSahRMgBQq5Bv0nMjOO7O8iylgBPPKw2jBeU9U4JV9UaTauzmunC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1111111111', 0.00, NULL, NULL, 0, '2020-07-16 22:08:31', '2020-07-16 22:08:31'),
(29, NULL, NULL, 'franchise', 'lSxVt02NHE', 'rficc@k6bc.com', NULL, '$2y$10$PFMD56p3HWJn8oGNG5BdM.dlwN1aWXKKO6APvmF6AK9VaSTmPS/J2', NULL, NULL, NULL, 'dZ2ofbi55i', NULL, NULL, NULL, NULL, 0.00, NULL, NULL, 0, '2020-07-31 23:03:02', '2020-07-31 23:03:02'),
(31, NULL, NULL, 'staff', 'Ritendra', 'dahalriten@gmail.com', NULL, '$2y$10$0gOgDiAzDpZYbGiAYRsIFeZcsgvt8JkzI8jRkIHUuXYoCeplB7dia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9841775336', 0.00, NULL, NULL, 0, '2020-08-10 16:26:33', '2020-08-10 16:26:33'),
(33, NULL, NULL, 'seller', 'Nep Shopy', 'nepshopy@gmail.com', '2020-08-18 21:59:40', '$2y$10$eUKRlkmm2TAug75cfGQ4i.WoUbcJ2uVPqUlVkox.cv4CCyGEIMQEm', NULL, NULL, 'uploads/TikH3rqAQoj791zPMYeek83dSdJwWMUU8mMIEZtI.png', NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, 0, '2020-08-18 21:56:46', '2020-08-18 22:05:21'),
(34, NULL, NULL, 'seller', 'ashish dulal', 'azizdulal.ad@gmail.com', '2020-08-18 22:34:23', '$2y$10$R0RTs3P0GaWyDj73/qzcDOb5zFikCb.OgE5g46tU206AXcCNK9TfS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, 0, '2020-08-18 22:33:50', '2020-08-18 22:34:23');

-- --------------------------------------------------------

--
-- Table structure for table `variations`
--

CREATE TABLE `variations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attribute_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variation_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variations`
--

INSERT INTO `variations` (`id`, `attribute_id`, `variation_name`, `created_at`, `updated_at`) VALUES
(14, '1', 'XS, S, M, L, XL, XXL, 3, 4, 5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 11, 11.5, 12', '2020-07-20 07:34:17', '2020-07-30 06:25:57'),
(15, '2', 'Cotton,Silk ,Linen ,Wool ,Leather Material,Georgette ,Chiffon ,Nylon ,Polyester ,Velvet ,Denim ,Rayon ,Viscose ,Satin ,Crepe ,Lycra ,Net or Lace,nideal', '2020-07-20 07:35:35', '2020-07-30 18:58:09'),
(16, '4', '2GB, 4GB, 6GB, 10GB, 16GB, 32GB, 64GB, 8GB', '2020-07-20 07:36:15', '2020-07-30 06:27:10'),
(17, '5', '1kg,2kg,3kg,4kg,5kg,6kg,7kg,8kg,9kg,10kg,11kg,12kg,13kg,14kg,15kg,20kg,25kg,50kg,100kg,500kg,1000kg', '2020-07-20 07:36:59', '2020-07-30 06:37:42'),
(19, '6', '10g,25g,50g,100,150,200g,250g,300g,400g,500g,600g,700g,800,900', '2020-07-23 06:38:00', '2020-07-30 06:29:53'),
(20, '7', '5\'\',4\'\',3\'\',2\'\',1\'\'', '2020-07-26 06:15:44', '2020-07-30 17:24:26'),
(21, '9', '32gb,64gb,128gb,256gb,512gb,1TB,2TB', '2020-07-30 06:41:32', '2020-07-30 06:41:32'),
(22, '8', '.5,1,1.5,2,3,4,5,6,7,8,9,10,15,20,25,50,100', '2020-07-30 06:42:50', '2020-07-30 06:42:50'),
(23, '11', '10g,20g,30g,40g,100g,200g,250g,300g,400g,500g,600g,700g,750g,800g,1kg,2kg,3kg,4kg,5kg,6kg,7kg,8kg,9kg,10kg,15kg,20kg,25kg,30kg,40kg,50kg,100kg,50g,900g', '2020-08-01 04:33:31', '2020-08-01 04:34:56'),
(24, '13', 'hello,world', '2020-08-02 23:34:13', '2020-08-02 23:34:13'),
(25, '14', 'Short, Long, Mini, Midi, Knee Length, Ankle Length, Floor Length, Above Knee, Maxi, Below Knee', '2020-08-04 15:08:04', '2020-08-04 15:08:04'),
(26, '15', 'Hooded, Peter Pan, Mandarin Collar, Notched, Scoop Neck, Square Neck, Shawl Collar, Boat Neck, Button-down, Slash, Lapel, Asymmetrical, Turn-down, Pillow Collar', '2020-08-04 15:08:43', '2020-08-04 15:08:43'),
(27, '17', 'Houndstooth, Herringbone, Splice, Cartoon, Brocade, Threadwork, Kashmiri Embroidery, Chikan Embroidery, Aari Embroidery, Phulkari Embroidery, Kantha Embroidery, Embroidery, Sequin and/or Foil Mirror Work, Patti Work, Semi Stitched', '2020-08-04 15:10:01', '2020-08-04 15:10:01'),
(28, '18', 'Shift Dresses, Swing Dresses, T-Shirt Dresses, Tulip Dresses, Wrap Dresses, Asymmetric Dresses, A-Line Dresses, Peplum Dresses, Formal Dresses, Shirt Dresses, Sweater Dresses, Tunic Dresses, Bodycon Dresses, Wedding Dresses', '2020-08-04 15:11:45', '2020-08-04 15:16:20'),
(29, '19', 'Raglan Sleeve, One Shoulder, Sleeveless, Long Sleeve, Short Sleeve, 3/4 Length, Off Shoulder, Butterfly Sleeve, Petal Sleeve, Lantern Sleeve, Batwing Sleeve, Cuff Sleeve, Flare Sleeve, Ruffle sleeve', '2020-08-04 15:15:26', '2020-08-04 15:15:26'),
(30, '29', 'Spring, Autumn, Summer, Winter, 2017, 2013, 2015, 2012, 2014, 2016, 2018, 2019, 2020', '2020-08-04 15:17:51', '2020-08-04 15:17:51'),
(31, '30', 'Basic, Contemporary, Korean, Denim, Fashion, Formal, Sexy, Club, Youthful, Preppy / Heritage, Basic Essentials, Business & Formal, Occasion & Evening, Fast Fashion / High street', '2020-08-04 15:18:44', '2020-08-04 15:18:44'),
(32, '32', '3/4 Sleeve, Long Sleeve, Sleeveless, Short Sleeve', '2020-08-04 15:20:28', '2020-08-04 15:20:28'),
(33, '33', 'Soft Net, 	Poly-cotton, 	Rayon, 	Serge, 	Aquatimo, 	Stretch Cotton, 	Chanderi Silk, 	Banaras Silk, 	Handloom Cotton, 	Kalamkari, 	Ikkat, 	Cambric Cotton, 	Raw Silk, 	Net Silk', '2020-08-04 15:22:14', '2020-08-04 15:22:14'),
(34, '31', 'Korean, 		Party, 		Military, 		Nautical, 		Athleisure, 		Retro & Vintage, 		Office, 		Minimalist, 		Street Style, 		Preppy / Heritage, 		Rock and Roll, 		Tropical Island, 		Bohemian', '2020-08-04 15:23:14', '2020-08-04 15:23:14');

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_details` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addons`
--
ALTER TABLE `addons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_settings`
--
ALTER TABLE `business_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_usages`
--
ALTER TABLE `coupon_usages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_packages`
--
ALTER TABLE `customer_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_products`
--
ALTER TABLE `customer_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flash_deals`
--
ALTER TABLE `flash_deals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flash_deal_products`
--
ALTER TABLE `flash_deal_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchises`
--
ALTER TABLE `franchises`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pickup_points`
--
ALTER TABLE `pickup_points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policies`
--
ALTER TABLE `policies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_stocks`
--
ALTER TABLE `product_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refund_requests`
--
ALTER TABLE `refund_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `searches`
--
ALTER TABLE `searches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sellers`
--
ALTER TABLE `sellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_withdraw_requests`
--
ALTER TABLE `seller_withdraw_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_settings`
--
ALTER TABLE `seo_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_category_id` (`category_id`);

--
-- Indexes for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sub_category_id` (`sub_category_id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_replies`
--
ALTER TABLE `ticket_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `variations`
--
ALTER TABLE `variations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addons`
--
ALTER TABLE `addons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `business_settings`
--
ALTER TABLE `business_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `coupon_usages`
--
ALTER TABLE `coupon_usages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customer_packages`
--
ALTER TABLE `customer_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer_products`
--
ALTER TABLE `customer_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `flash_deals`
--
ALTER TABLE `flash_deals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `flash_deal_products`
--
ALTER TABLE `flash_deal_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `franchises`
--
ALTER TABLE `franchises`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pickup_points`
--
ALTER TABLE `pickup_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `policies`
--
ALTER TABLE `policies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `product_stocks`
--
ALTER TABLE `product_stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=921;

--
-- AUTO_INCREMENT for table `refund_requests`
--
ALTER TABLE `refund_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `searches`
--
ALTER TABLE `searches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sellers`
--
ALTER TABLE `sellers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `seller_withdraw_requests`
--
ALTER TABLE `seller_withdraw_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `seo_settings`
--
ALTER TABLE `seo_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ticket_replies`
--
ALTER TABLE `ticket_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `variations`
--
ALTER TABLE `variations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
