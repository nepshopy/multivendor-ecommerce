@extends('frontend.layouts.app')

@section('content')

<section class="gry-bg py-4 profile">
    <div class="container">
        <div class="row cols-xs-space cols-sm-space cols-md-space">
            <div class="col-lg-9 mx-auto">
                <div class="main-content">
                    <!-- Page title -->
                    <div class="page-title">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                    {{__('Franchise Informations')}}
                                </h2>
                            </div>
                            <div class="col-md-6">
                                <div class="float-md-right">
                                    <ul class="breadcrumb">
                                        <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                        <li class="active"><a href="{{ route('franchise.apply') }}">{{__('Create Franchise')}}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form class="" action="{{ route('franchise.store_franchise_user') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @if (!Auth::check())
                        <div class="form-box bg-white mt-4">
                            <div class="form-box-title px-3 py-2">
                                {{__('User Info')}}
                            </div>
                            <div class="form-box-content p-3">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <!-- <label>{{ __('Name') }}</label> -->
                                            <div class="input-group input-group--style-1">
                                                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" placeholder="{{ __('Name') }}" name="name">
                                                <span class="input-group-addon">
                                                    <i class="text-md la la-user"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <!-- <label>{{ __('Email') }}</label> -->
                                            <div class="input-group input-group--style-1">
                                                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{ __('Email') }}" name="email">
                                                <span class="input-group-addon">
                                                    <i class="text-md la la-envelope"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <!-- <label>{{ __('Phone Number') }}</label> -->
                                            <div class="input-group input-group--style-1">
                                                <input type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="{{ __('Phone Number') }}" name="phone">
                                                <span class="input-group-addon">
                                                    <i class="text-md la la-phone"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <!-- <label>{{ __('Address') }}</label> -->
                                            <div class="input-group input-group--style-1">
                                                <input type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="{{ __('Address') }}" name="address">
                                                <span class="input-group-addon">
                                                    <i class="text-md la la-map"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <!-- <label>{{ __('Password') }}</label> -->
                                            <div class="input-group input-group--style-1">
                                                <input type="password" id="paswd1" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Password') }}" name="password">
                                                <span class="input-group-addon">
                                                    <i class="text-md la la-lock"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <!-- <label>{{ __('Confirm Password') }}</label> -->
                                            <div class="input-group input-group--style-1">
                                                <input type="password" id="paswd2" class="form-control" placeholder="{{ __('Confirm Password') }}" name="password_confirmation">
                                                <span class="input-group-addon">
                                                    <i class="text-md la la-lock"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-box-title px-3 py-2">
                                    <input type="checkbox" onclick="showPassword()">&nbsp;<label>{{__('Show Password')}}</label>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="form-box bg-white mt-4">
                            <div class="form-box-title px-3 py-2">
                                {{__('Basic Info')}}
                            </div>
                            <div class="form-box-content p-3">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>{{__('User Photo')}}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="file" name="avatar" id="file-2" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                        <label for="file-2" class="mw-100 mb-3">
                                            <span></span>
                                            <strong>
                                                <i class="fa fa-upload"></i>
                                                {{__('Choose image')}}
                                            </strong>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>{{__('Citizenship Front')}}<span class="required-star">*</span></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="file" name="citizenship_front" id="file-3" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                        <label for="file-3" class="mw-100 mb-3">
                                            <span></span>
                                            <strong>
                                                <i class="fa fa-upload"></i>
                                                {{__('Choose image')}}
                                            </strong>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>{{__('Citizenship Back')}}<span class="required-star">*</span></label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="file" name="citizenship_back" id="file-4" class="custom-input-file custom-input-file--4" data-multiple-caption="{count} files selected" accept="image/*" />
                                        <label for="file-4" class="mw-100 mb-3">
                                            <span></span>
                                            <strong>
                                                <i class="fa fa-upload"></i>
                                                {{__('Choose image')}}
                                            </strong>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-right mt-4">
                            <button type="submit" class="btn btn-styled btn-base-1">{{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@section('script')
<script>
    function showPassword() {
      var x = document.getElementById("paswd1");
      if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
    var x = document.getElementById("paswd2");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>
@endsection
@endsection
