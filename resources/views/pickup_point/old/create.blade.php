@extends('layouts.app')

@section('content')

<div class="col-lg-8 col-lg-offset-2">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Pickup Point Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('pick_up_points.store') }}" method="POST" enctype="multipart/form-data">
           @csrf
           <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>
                <div class="col-sm-9">
                    <input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="address">{{__('Location')}}</label>
                <div class="col-sm-9">
                    <textarea name="address" rows="4" class="form-control" required></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="address_address">Address</label>
                <div class="col-sm-9">
                <input type="text" id="address-input" name="address_address" class="form-control map-input">
                <input type="text" name="address_latitude" id="address-latitude" />
                <input type="text" name="address_longitude" id="address-longitude"/>
            </div>
            <div id="address-map-container" style="width:100%;height:400px; ">
                <div style="width: 100%; height: 100%" id="address-map"></div>
            </div>
        </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="phone">{{__('Phone')}}</label>
                <div class="col-sm-9">
                    <input type="text" placeholder="{{__('Phone')}}" id="phone" name="phone" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">{{__('Pickup Point Status')}}</label>
                <div class="col-sm-3">
                    <label class="switch" style="margin-top:5px;">
                      <input value="1" type="checkbox" name="pick_up_status">
                      <span class="slider round"></span>
                  </label>
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="name">{{__('Pick-up Point Manager')}}</label>
            <div class="col-sm-9">
                <select name="staff_id" class="form-control demo-select2-placeholder" required>
                    @foreach(\App\Staff::all() as $staff)
                    <option value="{{$staff->id}}">{{$staff->user->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="panel-footer text-right">
        <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
    </div>
</form>
<!--===================================================-->
<!--End Horizontal Form-->

</div>
</div>
@section('script')
    @parent
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDITiH9dyCKnt-UnF4o25afFXjWEslqYxI&libraries=places&callback=initialize" async defer></script>
    <script src="{{asset('/js/mapInput.js')}}"></script>
@stop
@endsection
