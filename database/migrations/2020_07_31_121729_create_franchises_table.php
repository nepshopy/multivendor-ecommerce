<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('franchises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('verification_status')->default("0");
            $table->text('verification_info')->nullable();
            $table->text('referal_code')->nullable();
            $table->integer('franchise_state_id')->nullable();
            $table->text('franchise_image')->nullable();
            $table->text('citizenship_front')->nullable();
            $table->text('citizenship_back')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchises');
    }
}
